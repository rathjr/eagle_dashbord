/**********************************************************************
 * Development By Group: ServeNak
 * Description: business account setting
 **********************************************************************/
import { Form, Button } from "react-bootstrap";
import React, { useEffect, useState } from "react";
import { viewUserbyID } from "../../../Redux/actions/userAction";
import { useDispatch, useSelector } from "react-redux";
import { updateUser, uploadImage } from "../../../service/user.service";
import Swal from "sweetalert2";
import { useHistory } from "react-router";
import { useParams } from "react-router";
export const BoSetting = () => {
  let user = JSON.parse(localStorage.getItem("user"));
  const [name, setName] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");
  const [password, setPassword] = useState("");
  const [profile, setProfile] = useState(
    "https://profiles.utdallas.edu/img/default.png"
  );
  const [browsedImage, setBrowseImage] = useState("");
  const viewUser = useSelector((state) => state.userReducer.viewUser);
  const dispatch = useDispatch();
  const history = useHistory();
  const [isUpdate, setIsupdate] = useState(false);
  useEffect(() => {
    dispatch(viewUserbyID(user.id));
    setName(viewUser.name);
    setPhoneNumber(viewUser.phoneNumber);
    if (viewUser.profile !== null) {
      setProfile(viewUser.profile);
    }
  }, []);
  console.log('viewuser : ',viewUser);
  const Loading = () => (
    // <ReactLoading className='loading' width='200px' type={"spokes"} color={"red"} delay={'1000'} />
    <span
      className="spinner-border spinner-border-sm mb-1 ml-2"
      role="status"
      aria-hidden="tru"
    ></span>
  );
  // const onUpdateUser = bindActionCreators(updateUserById, dispatch);
  const onUpdate = async (e) => {
    e.preventDefault();
    setIsupdate(true);
    let newUser = {
      name,
      phoneNumber,
      profile,
    };
    if (browsedImage) {
      let url = await uploadImage(browsedImage);
      newUser.profile = url;
    }

    updateUser(user.id, newUser).then((success) => {
      if (success) {
        setIsupdate(false);
        Swal.fire({
          icon: "success",
          confirmButtonText: "OK",
        }).then((result) => {
          if (result.isConfirmed) {
            history.push("/bo");
          }
        });
      }
    });
  };
  return (
    <div>
      <div className="content-wrapper">
        <div className="content-header">
          <div className="container-fluid">
            <div className="row mb-2">
              <div className="col-sm-6">
                <h2 className="m-0" style={{ fontWeight: "bold" }}>
                  ព័ត៏មានទូទៅ
                </h2>
              </div>
            </div>
          </div>
        </div>
        <section className="content h-100">
          <div className="container-fluid">
            <div className="row mb-2">
              <div className="col-7">
                <Form>
                  <Form.Group className="mb-3 ">
                    <Form.Label>ឈ្មោះ</Form.Label>
                    <Form.Control
                      onChange={(e) => setName(e.target.value)}
                      type="text"
                      value={viewUser.name}
                      type="text"
                      className="shadow p-1"
                      style={{ border: "0" }}
                    />
                  </Form.Group>
                  
                  <Form.Group className="mb-3">
                    <Form.Label>លេខទូរស័ព្ទ</Form.Label>
                    <Form.Control
                      onChange={(e) => setPhoneNumber(e.target.value)}
                      type="text"
                      value={viewUser.phoneNumber}
                      className="shadow p-1"
                      style={{ border: "0" }}
                    />
                  </Form.Group>
                
                </Form>
                <br />
                <div style={{ float: "right", marginBottom: "20px" }}>
                    <Button
                      onClick={onUpdate}
                      type="submit"
                      className="ml-4 mt-3"
                      style={{
                        width: "100px",
                        border: "0",
                        fontWeight: "bold",
                        backgroundColor: "#E30000",
                        color: "white",
                      }}
                      variant="outline-primary"
                    >
                      រក្សាទុក
                      {isUpdate ? Loading() : console.log("hello")}
                    </Button>{" "}
                  </div>
           
              </div>

              <div className="col-5">
                <Form.Group
                  controlId="formFileLg"
                  style={{
                    display: "flex",
                    justifyContent: "center",
                    alignItem: "center",
                  }}
                  className="mb-3 mt-4"
                >
                  <Form.Control
                    style={{ display: "none" }}
                    size="lg"
                    onChange={(e) => {
                      let url = URL.createObjectURL(e.target.files[0]);
                      setBrowseImage(e.target.files[0]);
                      url
                        ? setProfile(url)
                        : setProfile(
                            "data:image/svg+xml;base64,PHN2ZyB4bWxuczpkYz0iaHR0cDovL3B1cmwub3JnL2RjL2VsZW1lbnRzLzEuMS8iIHhtbG5zOmNjPSJodHRwOi8vY3JlYXRpdmVjb21tb25zLm9yZy9ucyMiIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyIgeG1sbnM6c3ZnPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB2aWV3Qm94PSIwIDAgNTAgNTAiIHByZXNlcnZlQXNwZWN0UmF0aW89InhNaWRZTWlkIG1lZXQiPjxtZXRhZGF0YT48cmRmOlJERj48Y2M6V29yaz48ZGM6Zm9ybWF0PmltYWdlL3N2Zyt4bWw8L2RjOmZvcm1hdD48ZGM6dHlwZSByZGY6cmVzb3VyY2U9Imh0dHA6Ly9wdXJsLm9yZy9kYy9kY21pdHlwZS9TdGlsbEltYWdlIi8+PGRjOnRpdGxlPkpkZW50aWNvbjwvZGM6dGl0bGU+PGRjOmNyZWF0b3I+PGNjOkFnZW50PjxkYzp0aXRsZT5EYW5pZWwgTWVzdGVyIFBpcnR0aWrDpHJ2aTwvZGM6dGl0bGU+PC9jYzpBZ2VudD48L2RjOmNyZWF0b3I+PGRjOnNvdXJjZT5odHRwczovL2dpdGh1Yi5jb20vZG1lc3Rlci9qZGVudGljb248L2RjOnNvdXJjZT48Y2M6bGljZW5zZSByZGY6cmVzb3VyY2U9Imh0dHBzOi8vZ2l0aHViLmNvbS9kbWVzdGVyL2pkZW50aWNvbi9ibG9iL21hc3Rlci9MSUNFTlNFIi8+PC9jYzpXb3JrPjwvcmRmOlJERj48L21ldGFkYXRhPjxyZWN0IGZpbGw9InRyYW5zcGFyZW50IiB3aWR0aD0iNTAiIGhlaWdodD0iNTAiIHg9IjAiIHk9IjAiLz48cGF0aCBmaWxsPSIjNTQ1NDU0IiBkPSJNMTMgMUwyNSAxTDI1IDEzWk0zNyAxTDM3IDEzTDI1IDEzWk0zNyA0OUwyNSA0OUwyNSAzN1pNMTMgNDlMMTMgMzdMMjUgMzdaTTEgMTNMMTMgMTNMMTMgMjVaTTQ5IDEzTDQ5IDI1TDM3IDI1Wk00OSAzN0wzNyAzN0wzNyAyNVpNMSAzN0wxIDI1TDEzIDI1WiIvPjxwYXRoIGZpbGw9IiNlOGU4ZTgiIGQ9Ik0xIDdMNyAxTDEzIDdMNyAxM1pNNDMgMUw0OSA3TDQzIDEzTDM3IDdaTTQ5IDQzTDQzIDQ5TDM3IDQzTDQzIDM3Wk03IDQ5TDEgNDNMNyAzN0wxMyA0M1oiLz48cGF0aCBmaWxsPSIjZDE5ODc1IiBkPSJNMTYgMTZMMjQgMTZMMjQgMjRMMTYgMjRaTTM0IDE2TDM0IDI0TDI2IDI0TDI2IDE2Wk0zNCAzNEwyNiAzNEwyNiAyNkwzNCAyNlpNMTYgMzRMMTYgMjZMMjQgMjZMMjQgMzRaIi8+PC9zdmc+"
                          );
                    }}
                    type="file"
                  />
                  <Form.Label
                    style={{
                      backgroundPosition: "center",
                      backgroundSize: "cover",
                      width: "600px",
                      height: "300px",
                      position: "relative",
                    }}
                    className="shadow p-1 mx-auto"
                  >
                    <img className="w-100" src={profile} />
                  </Form.Label>
                </Form.Group>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  );
};
