/**********************************************************************
 * Development By Group: ServeNak
 * Description: Form for business owner post their services
 **********************************************************************/

 import { Form, Button, FormControl, Table, Pagination } from "react-bootstrap";
 import React, { useEffect, useState } from "react";
 import { useDispatch, useSelector } from "react-redux";
 import { bindActionCreators } from "redux";
 import { uploadImage } from "../../../service/uploadImg.service";
 import { updatePostById, fetchPostById, fetchPlayerById, createPlayer, updatePlayer, fetchPlayer, deletePlayer, getPlayerByPaging } from "../../../service/post.service";
 import { useLocation } from "react-router";
 import Swal from "sweetalert2";
 import ReactPaginate from "react-paginate";
 import "../../../assets/Style/carousel.css";
 import query from "query-string";
 import { useHistory } from "react-router";
 import {
   onDeletePost,
   fetchAllPosts,
 } from "./../../../Redux/actions/postAction";
import { set } from "lodash";
import { createMatch, deleteMatch, fetchMatchById, getMatchByPaging, updateMatch } from "../../../service/match.service";
 
 let user = JSON.parse(localStorage.getItem("user"));
 const Match = () => {

  const btnvalidate = () => {
    if (name !== "" && result !== "" && dateOfMatch !== "" && stadium !== "" ) {
      return false  
    } else 
        return true 
}
   const posts = useSelector((state) => state.postReducer.posts);
   const dispatch = useDispatch();
   const onFetch = bindActionCreators(fetchAllPosts, dispatch);
   const [dateOfMatch, setDateOfMatch] = useState("");
   const [matchs, setMatchs] = useState([])
   const [meta, setMeta] = useState({})
   const [name, setName] = useState("");
   const [time, setTime] = useState("");
   const [type, setType] = useState("");
   const [lresult, setLresult] = useState("");
   const [stadium, setStadium] = useState("");
   const [result, setResult] = useState("");
   const [ename, setEname] = useState("Eagle FC");
   const [elogo, setElogo] = useState("http://3.144.75.173:17000/files/cc33c39f-59bd-4ed2-9728-f19aee3fd4d1.png");
   const [logo, setLogo] = useState("");
   const [browsedImage, setBrowsedImage] = useState("");
   const [isPost, setIsPost] = useState(false);
   const placeholder = "../images/defaultphoto.jpg";
   const history = useHistory();
   const { search } = useLocation();
   let { id } = query.parse(search);
   console.log('dateofmatch',dateOfMatch);
   const Loading = () => (
    // <ReactLoading className='loading' width='200px' type={"spokes"} color={"red"} delay={'1000'} />
    <span
      className="spinner-border spinner-border-sm mb-1 ml-2"
      role="status"
      aria-hidden="tru"
    ></span>
  );
   useEffect(async () => {
     if (search == "") {
       setName("");
       setResult("");
       setStadium("");
       setDateOfMatch("");
       setBrowsedImage("");
       setTime("");
       setType("");
       setLresult("");
     } else {
       const result = await fetchMatchById(id);
       setName(result.name);
       setResult(result.result);
       setStadium(result.stadium);
       setDateOfMatch(result.dateOfMatch);
       setBrowsedImage(result.logo);
       setTime(result.time);
       setType(result.type);
       setLresult(result.lresult);
     }
   }, [search]);

   const onDelete=(id)=>{
    deleteMatch(id).then((success)=>{
      if (success) {
        setIsPost(false);
        Swal.fire({
          icon: "success",
          confirmButtonText: "OK",
        }).then((result) => {
          if (result.isConfirmed) {
            let tmp = matchs.filter(item=>{
              return item._id!==id
      })
      setMatchs(tmp)
          }
        });
      }
    });
}

   useEffect(() => {
    const getMatchPaging = async () => {
        let result = await getMatchByPaging(1)
        setMatchs(result.data)
        console.log("all match",matchs);
    }
    const getMeta = async () => {
      let result = await getMatchByPaging(1)
      setMeta(result.metadata)
  }
  getMatchPaging()
    getMeta()
},[])
const onPageChange= async ({selected})=>{
  let result = await getMatchByPaging(selected+1)
  setMatchs(result.data)
}
console.log('mymeta',meta);
   //Browse image from and add to image tag
   function onBrowsedImage(e) {
     setLogo(e.target.files[0]);
     setBrowsedImage(URL.createObjectURL(e.target.files[0]));
   }
   console.log("search", search);
   //Add/Update  to api with image
   async function onSave() {
     setIsPost(true)
     if (search === "") {
       const url = logo && (await uploadImage(logo));
       console.log(url);
       const match = {
         name,
         result,
         stadium,
         ename,
         elogo,
         dateOfMatch,
         time,
         type,
         lresult,
         logo: url ? url : placeholder,
       };
       console.log(name,result,stadium,ename,elogo,dateOfMatch,logo);
       await createMatch(match).then((success) => {
        if (success) {
          setIsPost(false);
          Swal.fire({
            icon: "success",
            confirmButtonText: "OK",
          }).then((result) => {
            if (result.isConfirmed) {
              setName("");
              setResult("");
              setStadium("");
              setDateOfMatch("");
              setBrowsedImage("");
              setTime("");
              setType("");
              setLresult("");
              history.push("/bo/match");
            }
          });
        }
      });
     } else {
       const url = logo && (await uploadImage(logo));
       const match = {
        name,
        result,
        stadium,
        ename,
        elogo,
        time,
         type,
         lresult,
        dateOfMatch,
        logo: url ? url : placeholder,
      };
       await updateMatch(id, match).then((success) => {
        if (success) {
          setIsPost(false);
          Swal.fire({
            icon: "success",
            confirmButtonText: "OK",
          }).then((result) => {
            if (result.isConfirmed) {
              history.push("/bo/match");
            }
          });
        }
      });
    
     }
   }
 
   return (
     <div>
       <div className="content-wrapper">
         <section className="content h-100">
           <div className="container-fluid">
             <div className="row mb-2">
             <div className="col-3">
                 <Form>
                   <Form.Group className="mb-3" controlId="title">
                     <Form.Label>ឈ្មោះគូប្រកួត</Form.Label>
                     <Form.Control
                       value={name}
                       onChange={(e) => setName(e.target.value)}
                       type="text"
                       // value={title} onChange={(e)=>setTitle(e.target.value)}
                       className="shadow p-2"
                       style={{ border: "0" }}
                     />
                   </Form.Group>
                 
                   <Form.Group className="mb-3" controlId="title">
                     <Form.Label>លទ្ធផល</Form.Label>
                     <Form.Control
                       value={result}
                       onChange={(e) => setResult(e.target.value)}
                       type="text"
                       // value={title} onChange={(e)=>setTitle(e.target.value)}
                       className="shadow p-2"
                       style={{ border: "0" }}
                     />
                   </Form.Group>
                   <Form.Group className="mb-3" controlId="title">
                     <Form.Label>កីឡាដ្ខាន</Form.Label>
                     <Form.Control
                       value={stadium}
                       onChange={(e) => setStadium(e.target.value)}
                       type="text"
                       // value={title} onChange={(e)=>setTitle(e.target.value)}
                       className="shadow p-2"
                       style={{ border: "0" }}
                     />
                   </Form.Group>
                 
                   
                   
                 </Form>
               </div>
               <div className="col-4">
               <Form>
                   <Form.Group className="mb-3" controlId="title">
                     <Form.Label>លេងម៉ោង</Form.Label>
                     <Form.Control
                       value={time}
                       onChange={(e) => setTime(e.target.value)}
                       type="text"
                       // value={title} onChange={(e)=>setTitle(e.target.value)}
                       className="shadow p-2"
                       style={{ border: "0" }}
                     />
                   </Form.Group>
                 
                   <Form.Group className="mb-3" controlId="title">
                     <Form.Label>ប្រភេទលេង</Form.Label>
                     <Form.Control
                       value={type}
                       onChange={(e) => setType(e.target.value)}
                       type="text"
                       // value={title} onChange={(e)=>setTitle(e.target.value)}
                       className="shadow p-2"
                       style={{ border: "0" }}
                     />
                   </Form.Group>
                   <Form.Group className="mb-3" controlId="title">
                     <Form.Label>លទ្ធផលចុងក្រោយ</Form.Label>
                     <Form.Control
                       value={lresult}
                       onChange={(e) => setLresult(e.target.value)}
                       type="text"
                       // value={title} onChange={(e)=>setTitle(e.target.value)}
                       className="shadow p-2"
                       style={{ border: "0" }}
                     />
                   </Form.Group>
                 
                   <Form.Group className="mb-3" controlId="title">
                     <Form.Label>ថ្ងៃប្រកួត</Form.Label>
                     <Form.Control
                       value={dateOfMatch}
                       onChange={(e) => setDateOfMatch(e.target.value)}
                       type="date"
                       // value={title} onChange={(e)=>setTitle(e.target.value)}
                       className="shadow p-2"
                       style={{ border: "0" }}
                     />
                   </Form.Group>
                   
                 </Form>
                 
                 <div style={{ float: "right", marginBottom: "20px" }}>
                   <Button
                    disabled={btnvalidate()}
                     onClick={onSave}
                     className="ml-4 mt-3"
                     style={{
                       width: "100px",
                       border: "0",
                       fontWeight: "bold",
                       backgroundColor: "#DCDCDC",
                       color: "black",
                     }}
                   >
                     {search ? "កែប្រែ" : "បង្កើត"}
                     {isPost ? Loading() : console.log("hello")}
                   </Button>{" "}
                 </div>
               </div>
               
              
              
               <div className="col-4 p-0">
                 <div style={{ width: "600px" }}>
                   <label htmlFor="myfile">
                     <img
                       style={{ width: "400px" }}
                       src={browsedImage ? browsedImage : placeholder}
                     />
                   </label>
                 </div>
                 <input
                   onChange={onBrowsedImage}
                   id="myfile"
                   type="file"
                   style={{ display: "none" }}
                 />
               </div>
             </div>
             {/* search and table */}
             {/* <div className="row mb-4 ml-2">
               <Form className="d-flex">
                 <FormControl
                   type="search"
                   className="shadow p-1"
                   style={{ border: "0", width: "350px" }}
                   placeholder="ស្វែងរក"
                   aria-label="Search"
                 />
                 <Button
                   style={{
                     border: "red",
                     backgroundColor: "#E00000",
                     marginLeft: "-3px",
                     borderRadius: "0px 5px 5px 0px",
                   }}
                   variant="outline-success"
                 >
                   <i class="fas fa-search" style={{ color: "white" }}></i>
                 </Button>
               </Form>
             </div> */}
             <div className="row mb-2 ml-2">
               <Table striped bordered hover>
                 <thead>
                   <tr>
                     <th>#</th>
                     <th>Logoខ្លួន</th>
                     <th>ឈ្មោះខ្លួន</th>
                     <th>គូរប្រកួត</th>
                     <th>Logoគូរប្រកួត</th>
                     <th>លទ្ធផល</th>
                     <th>ថ្ងៃប្រកួត</th>
                     <th>កីឡាដ្ខាន</th>
                     <th>ម៉ោង</th>
                     <th>ប្រភេទ</th>
                     <th>ក្រោយ</th>
                   </tr>
                 </thead>
                 <tbody>
                   {matchs.map((item, index) => (
                     <tr key={index}>
                       <td className="align-middle">{item.id}</td>
                       <td className="align-middle">
                         <img style={{ width: "100px" }} src={item.elogo} />
                       </td>
                       <td className="align-middle">{item.ename}</td>
                       <td className="align-middle">{item.name}</td>
                       <td className="align-middle">
                         <img style={{ width: "100px" }} src={item.logo} />
                       </td>
                       <td className="align-middle">{item.result}​</td>
                       <td className="align-middle">{item.dateOfMatch}​</td>
                       <td className="align-middle">{item.stadium}​</td>
                       <td className="align-middle">{item.time}​</td>
                       <td className="align-middle">{item.type}​</td>
                       <td className="align-middle">{item.lresult}​</td>
                       <td>
                         <ul
                           style={{
                             margin: "0px",
                             padding: "0px",
                           }}
                         >
                           <li
                             onClick={() =>
                               history.push(`match?id=${item.id}`)
                             }
                             className="btn btn-edit btn-edit-hover"
                             style={{
                               backgroundColor: "white",
                               width: "35px",
                               height: "35px",
                               borderRadius: "35px",
                               padding: "6px",
                               borderColor: "#28a745",
                             }}
                           >
                             <i
                               class="fas fa-pen-nib"
                               style={{ margin: "auto" }}
                             ></i>
                           </li>
                           <li
                           onClick={()=>onDelete(item.id)}
                            //  onClick={() =>
                            //    onDelete(item.id).then((success) => {
                            //      console.log("success", success);
                            //      if (success) {
                            //        Swal.fire({
                            //          text: "អ្នកបានលុបដោយជោគជ័យ",
                            //          icon: "success",
                            //          confirmButtonText: "OK",
                            //        }).then((result) => {
                            //          if (result.isConfirmed) {
                            //            history.push("/bo/post-service");
                            //          }
                            //        });
                            //      }
                            //    })
                            //  }
                             className="btn btn-delete btn-delete-hover"
                             style={{
                               backgroundColor: "white",
                               width: "35px",
                               height: "35px",
                               borderRadius: "35px",
                               padding: "6px",
                               borderColor: "#dc3545",
                             }}
                           >
                             <i
                               class="fas fa-trash"
                               style={{ margin: "auto" }}
                             ></i>
                           </li>
                         </ul>
                       </td>
                     </tr>
                   ))}
                 </tbody>
               </Table>
             </div>
           </div>
         </section>
         <div style={{marginTop:"5px"}}>
           <ReactPaginate
 pageCount={meta.totalPages}
 onPageChange={onPageChange}
 containerClassName={"paginationBttns"}
 activeClassName={"paginationActive"}
  pageLinkClassName={"paginationActive"}
 disabledClassName={"paginationDisabled"}
       />
          </div>
       </div>
     </div>
   );
 };
 
 export default Match;
 