/**********************************************************************
 * Development By Group: ServeNak
 * Description: Form for business owner post their services
 **********************************************************************/

 import { Form, Button, FormControl, Table, Pagination } from "react-bootstrap";
 import React, { useEffect, useState } from "react";
 import { useDispatch, useSelector } from "react-redux";
 import { bindActionCreators } from "redux";
 import { uploadImage } from "../../../service/uploadImg.service";
 import { updatePostById, fetchPostById, fetchPlayerById, createPlayer, updatePlayer, fetchPlayer, deletePlayer, getPlayerByPaging } from "../../../service/post.service";
 import { useLocation } from "react-router";
 import Swal from "sweetalert2";
 import ReactPaginate from "react-paginate";
 import "../../../assets/Style/carousel.css";
 import query from "query-string";
 import { useHistory } from "react-router";
 import {
   onDeletePost,
   fetchAllPosts,
 } from "./../../../Redux/actions/postAction";
import { set } from "lodash";
 
 let user = JSON.parse(localStorage.getItem("user"));
 const PostService = () => {

  const btnvalidate = () => {
    if (name !== "" && number !== "" && position !== "" && placeOfBirth !== "" && height !== "" && weight !== "" && jointed !== "" && bestFoot !== "" ) {
      return false  
    } else 
        return true 
}
   const posts = useSelector((state) => state.postReducer.posts);
   const dispatch = useDispatch();
   const onFetch = bindActionCreators(fetchAllPosts, dispatch);
   const [name, setName] = useState("");
   const [size, setSize] = useState("");
   const [players, setPlayers] = useState([])
   const [meta, setMeta] = useState({})
   const [number, setNumber] = useState("");
   const [position, setPosition] = useState("");
   const [placeOfBirth, setPlaceOfBirth] = useState("");
   const [height, setHeight] = useState("");
   const [weight, setWeight] = useState("");
   const [jointed, setJointed] = useState("");
   const [bestFoot, setBestFoot] = useState("");
   const [images, setImage] = useState("");
   const [browsedImage, setBrowsedImage] = useState("");
   const [isPost, setIsPost] = useState(false);
   const placeholder = "../images/defaultphoto.jpg";
   const history = useHistory();
   const { search } = useLocation();
   let { id } = query.parse(search);
   const Loading = () => (
    // <ReactLoading className='loading' width='200px' type={"spokes"} color={"red"} delay={'1000'} />
    <span
      className="spinner-border spinner-border-sm mb-1 ml-2"
      role="status"
      aria-hidden="tru"
    ></span>
  );
   useEffect(async () => {
     if (search == "") {
       setName("");
       setNumber("");
       setHeight("");
       setWeight("");
       setJointed("");
       setPlaceOfBirth("");
       setPosition("");
       setBestFoot("");
       setSize("");
       setBrowsedImage("");
     } else {
       const result = await fetchPlayerById(id);
       setName(result.name);
       setNumber(result.number);
       setHeight(result.height);
       setWeight(result.weight);
       setJointed(result.jointed);
       setPlaceOfBirth(result.placeOfBirth);
       setPosition(result.position);
       setBestFoot(result.bestFoot);
       setSize(result.size);
       setBrowsedImage(result.images);
     }
   }, [search]);

   const onDelete=(id)=>{
    deletePlayer(id).then((success)=>{
      if (success) {
        setIsPost(false);
        Swal.fire({
          icon: "success",
          confirmButtonText: "OK",
        }).then((result) => {
          if (result.isConfirmed) {
            let tmp = players.filter(item=>{
              return item._id!==id
      })
      setPlayers(tmp)
          }
        });
      }
    });
}

   useEffect(() => {
    const getPlayersPaging = async () => {
        let result = await getPlayerByPaging(1)
        setPlayers(result.data)
        console.log("all player",players);
    }
    const getMeta = async () => {
      let result = await getPlayerByPaging(1)
      setMeta(result.metadata)
      console.log("meta",players);
  }
    getPlayersPaging()
    getMeta()
},[])
const onPageChange= async ({selected})=>{
  let result = await getPlayerByPaging(selected+1)
  setPlayers(result.data)
}
console.log('mymeta',meta);
   //Browse image from and add to image tag
   function onBrowsedImage(e) {
     setImage(e.target.files[0]);
     setBrowsedImage(URL.createObjectURL(e.target.files[0]));
   }
   console.log("search", search);
   //Add/Update  to api with image
   async function onSave() {
     setIsPost(true)
     if (search === "") {
       const url = images && (await uploadImage(images));
       console.log(url);
       const players = {
         name,
         number,
         height,
         weight,
         jointed,
         placeOfBirth,
         position,
         bestFoot,
         size,
         images: url ? url : placeholder,
       };
       console.log(images);
       await createPlayer(players).then((success) => {
        if (success) {
          setIsPost(false);
          Swal.fire({
            icon: "success",
            confirmButtonText: "OK",
          }).then((result) => {
            if (result.isConfirmed) {
              setName("");
              setNumber("");
              setHeight("");
              setWeight("");
              setJointed("");
              setPlaceOfBirth("");
              setPosition("");
              setBestFoot("");
              setSize("");
              setBrowsedImage("");
              history.push("/bo/player");
            }
          });
        }
      });
     } else {
       const url = images && (await uploadImage(images));
       const players = {
        name,
        number,
        height,
        weight,
        jointed,
        placeOfBirth,
        position,
        bestFoot,
        size,
        images: url ? url : browsedImage,
      };
       const result = await updatePlayer(id, players).then((success) => {
        if (success) {
          setIsPost(false);
          Swal.fire({
            icon: "success",
            confirmButtonText: "OK",
          }).then((result) => {
            if (result.isConfirmed) {
              history.push("/bo/player");
            }
          });
        }
      });
      
       onFetch();
     }
   }
 
   return (
     <div>
       <div className="content-wrapper">
         <section className="content h-100">
           <div className="container-fluid">
             <div className="row mb-2">
               <div className="col-3">
                 <Form>
                   <Form.Group className="mb-3" controlId="title">
                     <Form.Label>ឈ្មោះ</Form.Label>
                     <Form.Control
                       value={name}
                       onChange={(e) => setName(e.target.value)}
                       type="text"
                       // value={title} onChange={(e)=>setTitle(e.target.value)}
                       className="shadow p-2"
                       style={{ border: "0" }}
                     />
                   </Form.Group>
                 
                   <Form.Group className="mb-3" controlId="title">
                     <Form.Label>លេខអាវ</Form.Label>
                     <Form.Control
                       value={number}
                       onChange={(e) => setNumber(e.target.value)}
                       type="text"
                       // value={title} onChange={(e)=>setTitle(e.target.value)}
                       className="shadow p-2"
                       style={{ border: "0" }}
                     />
                   </Form.Group>
                   <Form.Group className="mb-3" controlId="title">
                     <Form.Label>តួនាទី</Form.Label>
                     <Form.Control
                       value={position}
                       onChange={(e) => setPosition(e.target.value)}
                       type="text"
                       // value={title} onChange={(e)=>setTitle(e.target.value)}
                       className="shadow p-2"
                       style={{ border: "0" }}
                     />
                   </Form.Group>
                 
                   <Form.Group className="mb-3" controlId="title">
                     <Form.Label>កន្លែងកំណើត</Form.Label>
                     <Form.Control
                       value={placeOfBirth}
                       onChange={(e) => setPlaceOfBirth(e.target.value)}
                       type="text"
                       // value={title} onChange={(e)=>setTitle(e.target.value)}
                       className="shadow p-2"
                       style={{ border: "0" }}
                     />
                   </Form.Group>
                   
                 </Form>

               </div>
               <div className="col-4">
                 <Form>
                   <Form.Group className="mb-3" controlId="title">
                     <Form.Label>កំពស់</Form.Label>
                     <Form.Control
                       value={height}
                       onChange={(e) => setHeight(e.target.value)}
                       type="text"
                       // value={title} onChange={(e)=>setTitle(e.target.value)}
                       className="shadow p-2"
                       style={{ border: "0" }}
                     />
                   </Form.Group>
                 
                   <Form.Group className="mb-3" controlId="title">
                     <Form.Label>ទំងន់</Form.Label>
                     <Form.Control
                       value={weight}
                       onChange={(e) => setWeight(e.target.value)}
                       type="text"
                       // value={title} onChange={(e)=>setTitle(e.target.value)}
                       className="shadow p-2"
                       style={{ border: "0" }}
                     />
                   </Form.Group>
                   <Form.Group className="mb-3" controlId="title">
                     <Form.Label>ចូលនៅឆ្នាំ</Form.Label>
                     <Form.Control
                       value={jointed}
                       onChange={(e) => setJointed(e.target.value)}
                       type="text"
                       // value={title} onChange={(e)=>setTitle(e.target.value)}
                       className="shadow p-2"
                       style={{ border: "0" }}
                     />
                   </Form.Group>
                 
                   <Form.Group className="mb-3" controlId="title">
                     <Form.Label>លេងជើង</Form.Label>
                     <Form.Control
                       value={bestFoot}
                       onChange={(e) => setBestFoot(e.target.value)}
                       type="text"
                       // value={title} onChange={(e)=>setTitle(e.target.value)}
                       className="shadow p-2"
                       style={{ border: "0" }}
                     />
                   </Form.Group>
                   <Form.Group className="mb-3" controlId="title">
                     <Form.Label>លេខឈុត</Form.Label>
                     <Form.Control
                       value={size}
                       onChange={(e) => setSize(e.target.value)}
                       type="text"
                       // value={title} onChange={(e)=>setTitle(e.target.value)}
                       className="shadow p-2"
                       style={{ border: "0" }}
                     />
                   </Form.Group>
                 </Form>
                 <div style={{ float: "right", marginBottom: "20px" }}>
                   <Button
                    disabled={btnvalidate()}
                     onClick={onSave}
                     className="ml-4 mt-3"
                     style={{
                       width: "100px",
                       border: "0",
                       fontWeight: "bold",
                       backgroundColor: "#DCDCDC",
                       color: "black",
                     }}
                   >
                     {search ? "កែប្រែ" : "បង្កើត"}
                     {isPost ? Loading() : console.log("hello")}
                   </Button>{" "}
                 </div>
               </div>
               
              
              
              
              
               <div className="col-4 p-0">
                 <div style={{ width: "600px" }}>
                   <label htmlFor="myfile">
                     <img
                       style={{ width: "400px" }}
                       src={browsedImage ? browsedImage : placeholder}
                     />
                   </label>
                 </div>
                 <input
                   onChange={onBrowsedImage}
                   id="myfile"
                   type="file"
                   style={{ display: "none" }}
                 />
               </div>
             </div>
             {/* search and table */}
             {/* <div className="row mb-4 ml-2">
               <Form className="d-flex">
                 <FormControl
                   type="search"
                   className="shadow p-1"
                   style={{ border: "0", width: "350px" }}
                   placeholder="ស្វែងរក"
                   aria-label="Search"
                 />
                 <Button
                   style={{
                     border: "red",
                     backgroundColor: "#E00000",
                     marginLeft: "-3px",
                     borderRadius: "0px 5px 5px 0px",
                   }}
                   variant="outline-success"
                 >
                   <i class="fas fa-search" style={{ color: "white" }}></i>
                 </Button>
               </Form>
             </div> */}
             <div className="row mb-2 ml-2">
               <Table striped bordered hover>
                 <thead>
                   <tr>
                     <th>#</th>
                     <th>រូបភាព</th>
                     <th>ឈ្មោះ</th>
                     <th>កំពស់</th>
                     <th>លេខអាវ</th>
                     <th>ទំងន់</th>
                     <th>តួនាទី</th>
                     <th>ចូលឆ្នាំ</th>
                     <th>កន្លែងកំណើត</th>
                     <th>លេងជើង</th>
                     <th>លេងឈុត</th>
                   </tr>
                 </thead>
                 <tbody>
                   {players.map((item, index) => (
                     <tr key={index}>
                       <td className="align-middle">{item.id}</td>
                       <td className="align-middle">
                         <img style={{ width: "100px" }} src={item.images} />
                       </td>
                       <td className="align-middle">{item.name}</td>
                       <td className="align-middle">{item.height}</td>
                       <td className="align-middle">{item.number}​</td>
                       <td className="align-middle">{item.weight}​</td>
                       <td className="align-middle">{item.position}​</td>
                       <td className="align-middle">{item.jointed}​</td>
                       <td className="align-middle">{item.placeOfBirth}​</td>
                       <td className="align-middle">{item.bestFoot}​</td>
                       <td className="align-middle">{item.size}​</td>
                       <td>
                         <ul
                           style={{
                             margin: "0px",
                             padding: "0px",
                           }}
                         >
                           <li
                             onClick={() =>
                               history.push(`player?id=${item.id}`)
                             }
                             className="btn btn-edit btn-edit-hover"
                             style={{
                               backgroundColor: "white",
                               width: "35px",
                               height: "35px",
                               borderRadius: "35px",
                               padding: "6px",
                               borderColor: "#28a745",
                             }}
                           >
                             <i
                               class="fas fa-pen-nib"
                               style={{ margin: "auto" }}
                             ></i>
                           </li>
                           <li
                           onClick={()=>onDelete(item.id)}
                            //  onClick={() =>
                            //    onDelete(item.id).then((success) => {
                            //      console.log("success", success);
                            //      if (success) {
                            //        Swal.fire({
                            //          text: "អ្នកបានលុបដោយជោគជ័យ",
                            //          icon: "success",
                            //          confirmButtonText: "OK",
                            //        }).then((result) => {
                            //          if (result.isConfirmed) {
                            //            history.push("/bo/post-service");
                            //          }
                            //        });
                            //      }
                            //    })
                            //  }
                             className="btn btn-delete btn-delete-hover"
                             style={{
                               backgroundColor: "white",
                               width: "35px",
                               height: "35px",
                               borderRadius: "35px",
                               padding: "6px",
                               borderColor: "#dc3545",
                             }}
                           >
                             <i
                               class="fas fa-trash"
                               style={{ margin: "auto" }}
                             ></i>
                           </li>
                         </ul>
                       </td>
                     </tr>
                   ))}
                 </tbody>
               </Table>
             </div>
           </div>
         </section>
         <div style={{marginTop:"5px"}}>
           <ReactPaginate
 pageCount={meta.totalPages}
 onPageChange={onPageChange}
 containerClassName={"paginationBttns"}
 activeClassName={"paginationActive"}
  pageLinkClassName={"paginationActive"}
 disabledClassName={"paginationDisabled"}
       />
          </div>
       </div>
     </div>
   );
 };
 
 export default PostService;
 