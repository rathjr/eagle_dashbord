/**********************************************************************
 * Development By Group: ServeNak
 * Description: SignIn Page
 **********************************************************************/
import React, { useState } from "react";
import { Form, Button, Container, Row, Navbar } from "react-bootstrap";
import { useParams } from "react-router";
import { handleLogin, handleRegister } from "../../service/auth.service";
import { useHistory } from "react-router";
import { useEffect } from "react";
import 'react-phone-number-input/style.css'
import PhoneInput from 'react-phone-number-input'
import '../../assets/Style/App.css'
import Swal from 'sweetalert2'
import ReactDOM from 'react-dom';
import GoogleLogin from 'react-google-login';
import "../../assets/Style/carousel.css";
import ReactLoading from 'react-loading';
const SignUp = () => {
  const history = useHistory()
  let param = useParams()
  const [roles,setRoles] = useState(['admin']);
  const [name, setName] = useState("")
  const [phoneNumber, setPhoneNumber] = useState("")
  const [password, setPassword] = useState("")
  const [isSignup,setIsSignup] = useState(false)

  const btnvalidate = () => {
    if (name !== "" && phoneNumber !== "" && password !== "") {
      return false  
    } else 
        return true 
}
  useEffect(() => {
    console.log('log phonenumber',phoneNumber,isSignup);
  }, []);

  // const onLogin = (e) => {
  //   e.preventDefault()
  //   setIsSignup(true)
  //   handleLogin(phoneNumber, password).then(
  //     response => {
  //       if (response.data.success && response.data.data.roles[0] == 'ROLE_USER') {
  //         setIsSignup(false)
  //         Swal.fire({
  //           icon: 'success',
  //           confirmButtonText: 'OK'
  //         }).then((result) => {
  //           if (result.isConfirmed) {
  //             history.push("/")
  //           }
  //         })
  //       } else if (response.data.success && response.data.data.roles[0] == 'ROLE_BO') {
  //         setIsSignup(false)
  //         Swal.fire({
  //           icon: 'success',
  //           confirmButtonText: 'OK'
  //         }).then((result) => {
  //           if (result.isConfirmed) {
  //             history.push("/bo")
  //           }
  //         })
  //       } else {
  //         setIsSignup(false)
  //         Swal.fire({
  //           icon: 'success',
  //           confirmButtonText: 'OK'
  //         }).then((result) => {
  //           if (result.isConfirmed) {
  //             history.push("/admin")
  //           }
  //         })
  //       }

  //       console.log('user', response.data.data);
  //     }
  //   )
  // }

  const handleGoogle = (res) => {
    console.log(res.profileObj);
    setName(res.profileObj.familyName)
    setPhoneNumber(res.profileObj.email)
    console.log(name,phoneNumber);
     handleRegister(name, phoneNumber, password,roles).then((response) => {
      if (response.data.success) {
        setIsSignup(false)
        console.log('calling failed');
        handleLogin(phoneNumber, password).then(
      response => {
        if (response.data.success && response.data.data.roles[0] == 'ROLE_ADMIN') {
          setIsSignup(false)
          Swal.fire({
            icon: 'success',
            confirmButtonText: 'OK'
          }).then((result) => {
            if (result.isConfirmed) {
              history.push("/bo")
            }
          })
        } 

        console.log('user', response.data.data);
      }
    )
      }else{
        handleLogin(res.profileObj.email, password).then(
          response => {
            if (response.data.success && response.data.data.roles[0] == 'ROLE_ADMIN') {
              setIsSignup(false)
              Swal.fire({
                icon: 'success',
                confirmButtonText: 'OK'
              }).then((result) => {
                if (result.isConfirmed) {
                  history.push("/bo")
                }
              })
            } 
    
            console.log('user', response.data.data);
          }
        )
      }
    })
  };
 
  const onSignUp = (e) => {
    console.log('firstname',name,phoneNumber,password,roles,isSignup);
    e.preventDefault();
    setIsSignup(true)
    handleRegister(name, phoneNumber, password,roles).then((response) => {
      if (response.data.success) {
        setIsSignup(false)
        Swal.fire({
          icon: 'success',
          confirmButtonText: 'OK'
        }).then((result) => {
          if (result.isConfirmed) {
            history.push("/sign-in")
          }
        })
      }else{
        setIsSignup(false)
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text:  response.data.message,
        })
      }
    })
  };
  const Loading = () => (
    // <ReactLoading className='loading' width='200px' type={"spokes"} color={"red"} delay={'1000'} />
    <span className="spinner-border spinner-border-sm mb-2 ml-2" role="status" aria-hidden="tru"></span>
  );
  return (
    <div>
      {/* Nabar */}
      <Navbar
        expand="lg"
        style={{ heigth: "50px", backgroundColor: "#151719", padding: "0" }}
      >
        <Container>
          <img
            style={{ heigth: "49px", width: "50px", margin: "auto" }}
            src="http://3.144.75.173:17000/files/cc33c39f-59bd-4ed2-9728-f19aee3fd4d1.png"
            alt=""
          ></img>
        </Container>
      </Navbar>
      <div style={{}}>
        <div
          className="container shadow p-3 mt-3 mb-2"
          style={{ borderRadius: "5px", backgroundColor: "#151719" }}
        >
          <h1
            style={{
              fontSize: "45px",
              fontWeight: "bold",
              marginTop: "20px",
              marginLeft: "220px",
              color:"white"
            }}
          >
            បង្កើតគណនី
          </h1>
          <Row>
            <div className="col-5">
              <Form>
                <Form.Group className="mb-3">
                  <Form.Label style={{color:"#9ca9b3"}}>លេខទូរស័ព្ទ</Form.Label>
                  <Form.Control
                    type="text"
                    onChange={(e) => setPhoneNumber(e.target.value)}
                  />
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicPassword">
                  <Form.Label style={{color:"#9ca9b3"}}>ឈ្មោះ</Form.Label>
                  <Form.Control
                    type="text"
                    onChange={(e) => setName(e.target.value)}
                  />
                
                  
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicPassword">
                  <Form.Label style={{color:"#9ca9b3"}}>ពាក្យសម្ងាត់</Form.Label>
                  <Form.Control
                    type="password"
                   
                    onChange={(e) => setPassword(e.target.value)}
                  />
                </Form.Group>
                
                <Button
                disabled={btnvalidate()}
                onClick={onSignUp}
                  type="submit"
                  value="submit"
                  variant="primary"
                  style={{
                    margin: "20px 0px 10px 10px",
                    width: "190px",
                    height: "45px",
                    backgroundColor: "#E30000",
                    border: "0",
                    fontSize: "25px",
                    borderRadius: "5px",
                    color:"white"
                  }}
                >
                  បង្កើត
                  {
                  isSignup ? Loading() : console.log('hello')
                }

                </Button>
                <h3
                  style={{
                    fontWeight: "bold",
                    color:"white",
                    marginTop: "30px",
                  }}
                >
                  ចូលគណនីជាមួយ
                </h3>
                 <div className="google">
         <GoogleLogin
    clientId="57012778688-ommqmv839vlc316m9468hpnc484p8uph.apps.googleusercontent.com"
    buttonText="Login"
    onSuccess={handleGoogle}
    onFailure={handleGoogle}
    cookiePolicy={'single_host_origin'}
    buttonText="Google"
    className="btnGoogle"
  />
        </div>
                
              </Form>
            </div>

            <div className="col-7">
              <img
                style={{ width: "100%" }}
                src="http://3.144.75.173:17000/files/8b6ed6af-3577-487f-9591-5492e8c8da40.jpg"
                alt=""
              ></img>
<div style={{display:"flex",justifyContent:"center"}}>
                {/* loading */}
                  </div>
            </div>
          </Row>
        </div>
      </div>
    </div>
  );
};
export default SignUp;
