/**********************************************************************
 * Development By Group: ServeNak
 * Description: SignIn Page
 **********************************************************************/

import React, { useEffect } from "react";
import { Form, Button, Container, Row, Navbar, Nav } from "react-bootstrap";
import { Link } from "react-router-dom";
import { useHistory } from "react-router";
import { handleLogin, handleRegister } from "../../service/auth.service";
import { useState } from "react";
import Swal from 'sweetalert2'
import GoogleLogin from 'react-google-login';
import ReactLoading from 'react-loading';
const Login = () => {

  let user = JSON.parse(localStorage.getItem('user'))

  const [roles,setRoles] = useState([]);
  const [name, setName] =useState("")
  const [phoneNumber, setPhoneNumber] = useState("")
  const [password, setPassword] = useState("")
  const [isSignup, setIsSignup] = useState(false)
  const btnvalidate = () => {
    if (phoneNumber !== "" && password !== "") {
      return false
    } else
      return true
  }
  const history = useHistory()
  useEffect(() => {
    console.log('log phonenumber',phoneNumber,isSignup);
      roles[0]="admin"
  }, []);
  const Loading = () => (
    // <ReactLoading className='loading' width='200px' type={"spokes"} color={"red"} delay={'1000'} />
    <span className="spinner-border spinner-border-sm mb-2 ml-2" role="status" aria-hidden="tru"></span>
  );

  const handleGoogle = (res) => {
    console.log(res.profileObj);
    setName(res.profileObj.familyName)
    setPhoneNumber(res.profileObj.email)
    console.log(name,phoneNumber);
     handleRegister(name, phoneNumber, password,roles).then((response) => {
      if (response.data.success) {
        setIsSignup(false)
        console.log('calling failed');
        handleLogin(phoneNumber, password).then(
      response => {
        if (response.data.success && response.data.data.roles[0] == 'ROLE_ADMIN') {
          setIsSignup(false)
          Swal.fire({
            icon: 'success',
            confirmButtonText: 'OK'
          }).then((result) => {
            if (result.isConfirmed) {
              history.push("/")
            }
          })
        }

      }
    )
      }else{
        handleLogin(res.profileObj.email, password).then(
          response => {
            if (response.data.success && response.data.data.roles[0] == 'ROLE_ADMIN') {
              setIsSignup(false)
              Swal.fire({
                icon: 'success',
                confirmButtonText: 'OK'
              }).then((result) => {
                if (result.isConfirmed) {
                  history.push("/admin")
                }
              })
            }
          }
        )
      }
    })
  };

  const onLogin = (e) => {
    e.preventDefault()
    setIsSignup(true)
    handleLogin(phoneNumber, password).then(
      response => {
        if (response.data.success && response.data.data.roles[0] == 'ROLE_ADMIN') {
          setIsSignup(false)
          Swal.fire({
            icon: 'success',
            confirmButtonText: 'OK'
          }).then((result) => {
            if (result.isConfirmed) {
              history.push("/bo")
            }
          })
        }

      }
    )
  }
  return (
    <div style={{ miniHeight: "100vh" }}>
      {/* Nabar */}

      <Navbar expand="lg" style={{ backgroundColor: "#151719", padding: "0px" }}  >
        <Container>
          <Nav.Link style={{ margin: "auto" }}>
            <img style={{ heigth: "50px", width: "50px", margin: "auto" }}
              src="http://3.144.75.173:17000/files/cc33c39f-59bd-4ed2-9728-f19aee3fd4d1.png">
            </img>
          </Nav.Link>
        </Container>
      </Navbar>

      <div>
        <div
          className="container shadow p-3 mt-4 mb-2"
          style={{ borderRadius: "5px", backgroundColor: "#151719",height:"540px"}}
        >
          <h1
            style={{
              fontSize: "45px",
              fontWeight: "bold",
              marginTop: "20px",
              marginLeft: "220px",
              color:"white"
            }}
          >
            ចូលគណនី
          </h1>

          <Row >
            <div className="col-5 mt-2 mx-4">
              <Form style={{ fontSize: "18px", fontWeight: "bold"}}>
                <Form.Group className="mb-3">
                  <Form.Label style={{color:"#9ca9b3"}}>លេខទូរស័ព្ទ</Form.Label>
                  <Form.Control
                    type="text"
                    onChange={(e) => setPhoneNumber(e.target.value)}

                  />
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicPassword">
                  <Form.Label style={{color:"#9ca9b3"}}>ពាក្យសម្ងាត់</Form.Label>
                  <Form.Control
                    type="password"
                    value={password} onChange={(e) => setPassword(e.target.value)}
                  />
                </Form.Group>
                <Button
                  disabled={btnvalidate()}
                  onClick={onLogin}
                  variant="primary"
                  type="submit"
                  style={{
                    margin: "20px 0px 10px 10px",
                    width: "190px",
                    height: "45px",
                    backgroundColor: "#E30000",
                    border: "0",
                    fontSize: "25px",
                    borderRadius: "5px",
                    color:"white"
                  }}
                >
                  ចូលគណនី
                  {
                  isSignup ? Loading() : console.log('hello')
                }
                </Button>
               
                <Button
                  // onClick={onLogin}
                  as={Link}
                  to="/sign-up"
                  variant="primary"
                  type="submit"
                  style={{
                    margin: "20px 0px 10px 10px",
                    width: "190px",
                    height: "45px",
                    backgroundColor: "#E30000",
                    border: "0",
                    fontSize: "25px",
                    borderRadius: "5px",
                    color:"white"
                  }}
                >
                  បង្កើតគណនី
                </Button>
                <h3
                  style={{
                    fontWeight: "bold",
                    color:"white",
                    marginTop: "30px",
                  }}
                >
                  ចូលគណនីជាមួយ
                </h3>
                <div className="google">
         <GoogleLogin
    clientId="57012778688-ommqmv839vlc316m9468hpnc484p8uph.apps.googleusercontent.com"
    buttonText="Login"
    onSuccess={handleGoogle}
    onFailure={handleGoogle}
    cookiePolicy={'single_host_origin'}
    buttonText="Google"
    className="btnGoogle"
  />
        </div>

              </Form>
            </div>

            <div className="col-6" >
              <img
                style={{ width: "100%",height:"90%" }}
                src="http://3.144.75.173:17000/files/3a8d97ee-1851-4e8a-804e-c3c7a0db6d23.jpg"
              ></img>
            </div>
          </Row>
        
                
       
        </div>
      </div>
    </div>
  );
}
export default Login;