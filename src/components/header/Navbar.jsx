import React from "react";
import { Nav, Navbar, NavDropdown, Container } from "react-bootstrap";
import { Link } from "react-router-dom";
export default function NavMenu() {
  return (
    <div>
      <Navbar  expand="lg" style={{fontSize:"22px",fontWeight:"bold",backgroundColor:"white"}}>
        <Container>
          <img
              style={{ width: "50px",height:"50px" }}
              src="../images/servenaklogo.png"
              className="img-circle mr-2"
              alt="User Image"
          />
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="me-auto" >
              <Nav.Link as={Link} to="/home" href="#home" style={{marginLeft:"10px"}}>ទំព័រដើម</Nav.Link>
              <Nav.Link as={Link} to="/home/aboutus" href="#about-us" style={{marginLeft:"10px"}}>អំពីយើង</Nav.Link>
              <Nav.Link as={Link} to="/home/contact" href="#contact" style={{marginLeft:"10px"}}>ទំនាក់ទំនង</Nav.Link>
            </Nav>
          </Navbar.Collapse>
          <Navbar.Collapse id="basic-navbar-nav" className="d-flex justify-content-end">
            <Nav>
            <NavDropdown title="គណនីរបស់អ្នក" id="basic-nav-dropdown">
                <NavDropdown.Item style={{ fontWeight: "bold", fontSize: "17px" }} as={Link}to="/home/choose">
                  បង្កើតគណនីថ្មី
                </NavDropdown.Item>
                <NavDropdown.Item style={{ fontWeight: "bold", fontSize: "17px" }} as={Link} to="/home/signin">
                  ចូលគណនី
                </NavDropdown.Item>
              </NavDropdown>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </div>
  );
}