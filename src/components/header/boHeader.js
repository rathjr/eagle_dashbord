/**********************************************************************
 * Development By Group: ServeNak
 * Description: business dashboard navbar
 **********************************************************************/

import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
// import { fetchServiceByUID } from "../../Redux/actions/serviceAction";
import { Link } from "react-router-dom";
let user = JSON.parse(localStorage.getItem("user"));
let removeItem = () => localStorage.removeItem("user");
const BoHeader = () => {
  const dispatch = useDispatch();
  // useEffect(() => {
  //   dispatch(fetchServiceByUID(user.id));
  // }, []);
  return (
    <div>
      <nav
        className="main-header navbar navbar-expand "
        style={{ backgroundColor: "#151719", color: "white" }}
      >
        <ul className="navbar-nav">
          <li className="nav-item">
            <a className="nav-link" data-widget="pushmenu" role="button">
              <i style={{ color: "white" }} className="fas fa-bars" />
            </a>
          </li>
        </ul>
        <div
          className="d-flex justify-content-center"
          style={{ fontSize: "25px", fontFamily: "Poppins" }}
        >
          <img
            src="http://3.144.75.173:17000/files/cc33c39f-59bd-4ed2-9728-f19aee3fd4d1.png"
            alt="logoimages"
            style={{ height: "40px", padding: "0px", alignItems: "center" }}
            className="img-circle"
          />
          {}
          <span className="ml-2">Eagle FC</span>
        </div>
        <ul className="navbar-nav ml-auto">
          {/* change service */}

          {/* <li className="nav-item dropdown​ ">
            <a className="nav-link" data-toggle="dropdown" href="#">
              <span style={{ color: "white" }}>ប្ដូរសេវាកម្ម</span>
              <i style={{ color: "white" }} className="fas fa-caret-down" />
            </a>
            <div className="dropdown-menu dropdown-menu-lg dropdown-menu-right">
              <div className="dropdown-divider" />
              {viewService.map((service) => (
                <a href="#" className="dropdown-item">
                  {service.name}
                </a>
              ))}
            </div>
          </li> */}

          <li className="nav-item">
            <a
              className="nav-link"
              data-widget="fullscreen"
              href="#"
              role="button"
            >
              <i
                style={{ color: "white" }}
                className="fas fa-expand-arrows-alt"
              />
            </a>
          </li>
          <li className="nav-item dropdown">
            <a className="nav-link" data-toggle="dropdown" href="#">
              <i
                class="fas fa-bars"
                style={{ color: "white", fontSize: "20px" }}
              ></i>
            </a>
            <div className="dropdown-menu dropdown-menu-lg dropdown-menu-right">
              <div className="dropdown-divider" />
              <Link to="/bo/bo-setting">
                <a href="#" className="dropdown-item red-hv">
                  ការកំណត់
                </a>
              </Link>
              <Link to="/sign-in" onClick={removeItem}>
                <a href="#" className="dropdown-item red-hv">
                  ចាកចេញ
                </a>
              </Link>
            </div>
          </li>
        </ul>
      </nav>
   
    </div>
  );
};

export default BoHeader;
