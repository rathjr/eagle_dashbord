import React, { Component } from "react";
import { Form, Button, FormControl } from "react-bootstrap";
import Slider from "react-slick";
import { Card, Container, Row } from "react-bootstrap";
import { InputGroup } from "react-bootstrap";
import { Nav, NavDropdown } from "react-bootstrap";
import { Link } from "react-router-dom";
export default class Body extends Component {

    constructor() {
        super();
        this.state = {
          car: [
            {
              image:
                "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBQWFRYVFRYZGRgaHBgcGRocGhkcGBwcHBwZGRocGRgcIy4lHCQrHxgaJzgmKzA0NTU1GiQ7QDszPy40NTEBDAwMEA8QHhISHzQnJSs0MTQxNDQ0NDY0NjQ0NDU0NjQ9NDQ0Nj80MTE0NDY0NDQxNDE0NDQ0NDQ0NDE9NDU0NP/AABEIALUBFwMBIgACEQEDEQH/xAAcAAEAAQUBAQAAAAAAAAAAAAAABAIDBQYHAQj/xABHEAACAQICBgYGCQEGBAcAAAABAgADEQQhBRIxQVGBBiJhcZGhBxMycrHBFEJSYoKSotHwgzM0k7LC4RVz0vEWIyRDRITi/8QAGQEBAQEBAQEAAAAAAAAAAAAAAAECAwQF/8QAJBEBAQACAgICAgIDAAAAAAAAAAECESExAxJBUWGhcZETIjL/2gAMAwEAAhEDEQA/AOyxOSdIekOlU0l9Hw9ZFVywRaiJqdVdaxbVLC5VwM87CT06T6dpf22ApVl2XpOFJ5azH9Il1Vkt6dNic2T0qag/9Vo/F0eJC6yjtu4TKZPA+lHRVSw9eUJ3PTdfFgCvnIjdomKwXSHB1v7LE0XPBaiE+AN5lIHsREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQOVelrCNTeji0HWRlYe8hDDlZSPxzcsJiEq00dDdHVWB7GAZT5iedOdH+uwjgC7DMd+4fm1fCar6M8cXwnqybvQdqeeZZPbQ/law4hCNwnXx3nTeF1W20nZTntBtf+bj/OyLpLRFCqb1KFOoGuRrU0Yg26ygkXFwLjuMnOAwv5cRtt38J5TGVicjbVPmOYOwztx275ayn5alW6CaOqe1hwrDejOtwdjaqtq7s8toPESMno+RP7vjMZQ4BavVHcAFPnNvqgg6w2jdsGe0dx29htwl9CCARs/mR7YuGN508mcsrUE0Vpql/ZaTWpb6takLHvcB2l5dN6epe3hcNiBxp1Cjc9ds+Qm1gT3VmL4sWfatZHpDrJ/eNF4tOJpgVV/NZRJWF9KGi2Oq1ZqTbNWpTcEd5AIHMzOhZRXwyOLOiuODKGHgZm+L6q+y9gekmCrZUsTRc8FqLrflveZYGaRi+hWjqgs2EpD3F9WfFNWQV9HmGT+71sVhv+VXZRz1gfjMXx2G3RYnPv8AgmkqVjR0q5XcK9Gm9/xltY+Ev08Zp2ntTA4gbtVqlN25t1QeyS41vVb3E0kdMMZT/vGisSttvqGp4jyUibFoLTFPFUvW0w62YqyOurURl2q6HYbEHuImUZSIiAiIgIiICIiAiIgIiICIiAiIgIiICIiBarUgysp2MCDzFpx/o6xwulauHbJa6sB79Ml1sd1lZ1Has7JOSelTDtQxFHGUxmjK/C+qblb8CA9/emsbq7WXl0JBrDK2sNo2Bu0cD85adtW7Wup9td6njb4+Mt4aurqrKcmAZGHAi4I5GTEqhjY2DbjuP+89F4/h2l1wjVbG36TuPYZaR9U3tkfaG8HiP58M5FfDldo6u/7vb3fCWivHkfkf3m5Zr8GWMynCUBvE9CyzQbVyOz4SXqzF4ea46WwsrCyoCegTNqKdWUVzqqx4CXrQy3BBG4/vJvlZ21UUmYk1ioZidpbWA4X27LZCw4CX6NZqZFi2rlle4Nt97A887b5GrU2dWbMuSLcbKwy56v6psOHpKyLcXBA8beRnrzsxnP8ATr7cpeFxSuoPEAg/zwmIoH1GkGXYmLTWHAV6ACuO9qRQ/wBFu2GptQbLNDnfh22+IG0ZjMZ0dKFZsOK9MEvh2WugGZJp311FtutTNRO9hPFnhJzOkyk7jaYkfC4haiJUQhkdVZWGwqwDKR3giSJyZIiICIiAiIgIiICIiAiIgIiICIiAiIgJqfpD0b67CObXK5juNsh3kKOZm2SxiqAdGQ/WBHjA536OdIipg0Rj16LNRJ4hLFD+RlF/uzaWXj4zm3RQ/R9I18Mx1VqqSgOzXp5hbe4xuR9nw6HTxBXI5jeNons8f+2O4643hKw+OZTqvmNx/wB/lLjUBtp5rvXeO4cOzw4S0hVxkeR2cj/O+UsjKbjI+X87R5yWTfHFa/hWltm7zHdL9J7ZHZuPZI3rFbb1W/nDb3iVU6hBs2zc24d9viPCMomWO5yyFotLVJ94zXszt2i23ukgC+YnK8OGWOngEqAnoEqEzaywGPorTOeSkm3AE7idw3X3WGyScNWUXtusbXyK5XNt2Z2jLKZOtQV11WFxMcmhQp6rdXgRc917gHmDznWeTG46yvLU6TqtMMpB2Ec+w94mP0YWVnpN9XNeFtlv5uYcJliLWkOogDht/s+JNvl4TnjeLG5f2xnQ99QVsGduGeyf8ip/5lG3EBSyX40jNlmo46r6jHYevsSqPo1Tva70GPc6uv8AWm3znlNVkiIkCIiAiIgIiICIiAiIgIiICIiAiIgIiIHHPSZROGxlHGKDZXVyBvW511/EC/ICbuoDC6HKwPI5gyP6TdGCrhC1rlL+B2/C34pgugWkHqYKnfrmkWpOL2dSmS6rbxqFMj47p6PBlzY3hrqti8jJFLFsuRGssopsGBKnW45WYe+v7eEoNPgf2nqurxW7LEtqKvmjWP2T8jtEo9ay9Vx4/wAzkRlzvmDxH8+MlU8WwFnAde4X8NhmLjZ+UmS9SyOsjW4g7D3yXTxNj1l1Tv3qf2/mciU6dNs0bVPA7vHMeMqNOquwBhwvY+FvlznLKS8Us2yqMDsldpiqVYXzVlPd+1xJaVWG+/fkZyuFjlcdJgE9kQ41R7WXw8ZcXEKRcMCOwiYuN+iY7V1TukaqAe8lbdwYW+Jnlate9rW78/58eyRaDdbWINgL3N8ySMs+7+bJ0mN1tu8aQtOYD6RSqUwdVmTqN9morFqbfhcK3KZXo/pH6Rh6Va2qzKNdfsuvVqKfddWXlI9IXUHcc+WefM3POYzoxiQmKxWG+q5GIpj3rLiFHdUAb+t4zyY/P0ZY8bbdEROTBERAREQEREBERAREQEREBERAREQERECLpDDipTdPtKQO/cfG0490HqGhjcThTezrroODJkQBxKMpPuztU4z0/p/Q9I0cWBZQ6u2V+qx1amziGYfhmsMtZSrO281KQJ1sww2MMmH7jsNxHryPbAIP112H31/7jtElMv4h/Nh3y3qDd4bDPdLK68xQUyupy5lfmV78xLbZbeqTs2WPcdh+Mpeky5pkfs/VPLd3jzlmhpBHJVhqPsZW3nmLNNyXuclm5tJJ4jmNo+cvUcayjJg44Hb4yGyEeyT3W1h+U5j8Jt2SjXJzI2bSBrKPDrDmJbjMpyzPuVmqWPRva6p+9mPHbJGoDmPEG4/cTXVcbRmOKsGH/UO4S6tbMWYKey6t+U7fCcsvD9NTJnAgJ2i/hfvB285aqaMoMbtTUniFz8QPnIa411yOqy/esreIylxNJrezqy32WF/AqetyE5emc6/RuXpcOjaaZpS7LkC/ixE89QXyeyqDYKCSW78gBwsAe+eNjqV/bt2HI+BF5S2KT7Q/UPlLrP523Juc1JrsApNxsyF7Z8Jq2NR6Bp403tQqde+V6FVtSrf3QyP/AE+czzY1DtJPaFY/AZSJjXFdTSYDUYFWU5llIsRbdkTt7O6X1y9bNds2d7rcImv9DcUzYcU6hvUw7NQqHexp5Kx96mUf8U2CeNwIiICIiAiIgIiICIiAiIgIiICIiAiIgJoXpX0UKuF17ZpcHuIJvyGtzab7IOl8J62jUp2vrKbDtGa+YEDSugelDWwVEsesgNJs7nWp9XM8SoU/imwOoP775zj0eYj1WJxOEY5Naol+KnUfmwKHlOigkdons8d3jK6TP7eMp3jWHnI9fDI+3VbsYA27juktXB3jnlD0we/iJ0lsrXFQ/VnYR85TUpZgsD7wyYDsI2jykvVYdo/m6ea9wV3cNh8NomvauWXjy7x7QSgY7mPH2X5Mu2eWOy+XBlv5r8TeXa2HDAAEiwy2fICXqCAC1Rrjc2ZI7wBs75q3USZZT/qIgW2xPyPby6soNcHqsaij7ykr+Ygypqqa2rrZn2bdYN3b79nheeq53EHucqfytcSu01UTEYhBZVdzwAAby1cpQvYjDki/6tbymRLtvLj8KsPECWWAPtMx/B/+ZqVuX40owtMtdjkovrXJYN2DWzv2ADtMkYdlS+qLknIcDwkSri6KZtUVO12VBbmVEh1Okmjk6z4ukFG5W125LTBMxlljO6xlftktBVmpY46wsmKUi+416AuLD71In/BE3icd6Q9P9HtTX1Ls1Sk9OpQsjKodGDWYtawZdZT2Ods61gsUtWmlVDdKiq6nirAMPIz5/lsuVscrd1JiInNCIiAiIgIiICIiAiIgIiICIiAiIgIiIHEem1L6HpOliBkuv1jsGo/Vc8g4A9ybytdlNjcTH+l3RXrMOtQDNbgnxI5AFz4TmNP0k41EWnq0jqqq3ZGZiVAF2Ota+XCejw+WYyytY5amq7KuIB2gHtH8+Rl5HU7DyOR8/wB5wPEdN8e5J9fq9ioi+YW/nMZidO4t/bxNZuw1Ht4XtNXz4/Eq7k6fSL11T22Ce8Qv+aYvF9KMCtw+JoG27XRiOS3M+cyb5nM8TtlM53zX4h7u54j0g6MW9qzP2Cm582UfGYXE+kzCi/q6eI7LlFB5EsROTXnl4/z5/CXKui1fSnV+rh07CzEnnqgSFiPSHpFwGCIoUGzLTZgo32LswAmkBCdx8JOr46u+TVCRw1rA94G3hnM3y535TdT63TDHPtxDj3QqHkVAMxuI0riHyetVf3nZviZaXDE/7A/O0u/QiBc5DiSAJm55Xum6hWnszFDQztu+NvOSF0bTX2qqD3TreSXmUYAIeBn0F6FdLGrgTRbM4dygzudRuut+4lgOxRwnI6VPDAjJ3PYAB4sb+U2rQmICI3q6YQki92dr22XHUG874HeInF20vVvcOQNllPwCgk/mlaaYrrcB3HA6xF/FmPlA7LE48vSHFDZVftFzc9xJUeUvJ0nxA/8AdYj3yxB91V2c4HW4nKV6V4gbajdhY0wp/UzX5STT6SY4+yb9hU2Pu1H1VMDpkTR8PpfGn2mpqPd1m5qLDwYzILpatvb9K/tLpdNoia2NL1eI8BK10vU+74f7xo02GJgxpd+C+B/eVf8AFmtchbcwPjGjTNxNfXpCGYIoDudgW5593bM3RYlVLCxIBIvextmL75EXYiICIiAiIgYzT+CFbD1aZF7qSBxIztztbnPljTOG9XWdDxPxsT4gnnPrqfPXpD0Ky6RSkp1VquFU2yUsQAe7VZMvumBz4ITsB8J76s9g5ibxivR7WVivrUa3a6/6TI69D6ouF9WxG21S553Al0NRFLt8AT+0uLhSdx8hNhr6Kek+rUVBkCV9bTU2N7Zs2Ww7t0rTRuIZQyUwVN7MoFQHO2RDEHMeUDAJgSdw8z5SUujGG3q99kHibSfU0bit61R2Cm6ryAFhykR9HODdhnxYG/iVkAYSmD13Hb7TeYFj4ytXoi1lZuaJ8zfylBwrG1vJsuQsLSoYV94J5IT4kgwKvpdvZRB2nXax7SOr5SNi29ZbWtlstqA9uwAkS59Ff7LeXynrYZhbqse5GHibZwPKtYt7RBPbuv35c5Qo/lwfNN/dKxSf734r/MGSsPgKjZhCeFgi+bWED3CUr5nzYL331xlNlwrKAANTMbAWfzDFV8JGwWiHG0oO06zEeDL8TM5RwvF3PZrEL8b+cqoy08szYbiXVQOwaijzMkUsMTsUseKIQLe85IPfeTaGHRTrKihvtWGse9tpkoGNGkBMHx1F7yXP5RkPGXlwtPfdvBB+nPzksIvAeErVF4S6FuiFX2FVe0DP8xzl/Xvtz784FNeHxlxKQ3AwoqjgPAStUXgPAS09Wmguziw25g+ezzjBYxqpH0eg9XP2gLJ/iNZPAtAk+ryvkBxJ1R4yzUxlNPrs3uswXmxPwvJ+C6H1n1WxFaxtmE6zfncWHdqmbHgNBYejYpTBYfWbrPyY+zysJNptqGAbG1WdUw+qgYBHckKV1QSSzXLdY26qkZTM4foozWOIrs/3Euqjs1jdjy1ZtcRtEPAaPpUV1aSKgO2wzJ4sTmx7TJkRIEREBERAREQEwWnejOHxTU3qhg9NlZWUgNdblbmxyBJmdiBha/Ryi2d3B4hgT+oETE/+A8OrmpTIVze7mmhY323K6t7zcIgc70r6M0ruHer1goW6hkyBJAyc/aO6V4foLUp0fUIyMlmF2dtYhixN2Cn7RnQYgcbpeinEoRq1+qCMlOdr7A1l3dk2RtA4obKRPc6fN50CI2ObVNC4j61B/BW/ykyDV0OfrYZz/wDXc+epOrRLtduP1NF0x7VHV70ZPkJFfCYYHagPvkeRadqnhAMbNuLDB0D7L+DqZWuATc556p+AE6/UwNJvapoe9VPxEh1OjuDb2sLQP9JP2jZtzBcIR9b9P+8rWmw4HxH7zobdEMAf/jUx7o1f8tpYfoPgDspMPdq1R/rjZtowLcB4n9pWKv3T+n95t1ToBgzsNdfdrVPmTLB9HWH3YjFD+rf4rGzbXEq3IGqb8vkZ4ccgd0ZiHpgF11TcA7M/ZPjNmp+j+kpuMTiv8Rf+mSqXQfBaxeorVXNrtUckm2zWC2DdxvGzbR6el2qNq4em9VuCqWI7yLKvMzNYTorjq1jWdaCfZyqP+UWRfOb/AIfDoihURUUbFUBQO4DKXo2ba5o7odhKZDOprOPrVTr2P3U9leQmwqoAsMgJXEiEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQEREBERAREQERED//Z",
              title: "សេវាកម្មជួសជួលកុំព្យូទ័រ",
              desc: "ជួសជួលកុំព្យូទ័រពេញចិត្ដណាស់",
            },
            {
              image:
                "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBESEhISERERERERERERERERERIRERERGBQZGRgUGBgcIS4lHB4rHxgYJjgmKy8xNTU1GiQ7QDszPy40NTEBDAwMEA8QHhISHjQhISE0NDQ0NDQxNDQ0NDQ0NDQxNDQ0NDQ0NDQxND00NDQ0NDQ0NDQ0NDQ0NDQ0NDQxNDQ0NP/AABEIALcBEwMBIgACEQEDEQH/xAAbAAABBQEBAAAAAAAAAAAAAAACAAEDBAUGB//EAEIQAAIBAgMEBwMJBgUFAAAAAAABAgMRBBIhBTFBcQYiMlFhkaETgbEHFEJSYsHR4fBDcoKSstIzk6KjwhUjVHPi/8QAGQEBAAMBAQAAAAAAAAAAAAAAAAECAwQF/8QAIhEBAAMAAgIDAQADAAAAAAAAAAECEQMxEiEyQVEEExQi/9oADAMBAAIRAxEAPwDFiT4Z9Yromw/aR5dflDvVNoLrsq4ZasuY9dZlOh2jur057R7SzspIuUyrUgtHfUs0uBjz9LcKZRCSHiHFHI6DQFUqxjvZXxNa2i3ldq8W3qzenDvuWdr56hajiossQmnuZk4eSZPnyvqsvbgj6RHJP20xoojoVMyJbnPNcnGsTpLeHieAMVqh8bw5HRwdsuXpmNast7Kf/cXNFWG9lrZv+IuaOme2EdPSsGupHkWLEOD7EeRYLqAsOhCBhMSE0JAIQ9hgk1hmOJgCwQhmAEiHC9pksyPCdpgXBpCGkEKzixBiI0eUomw/aRTwlTNFFug+sjzMyzuj3CDaHaKdKLuXscuugI01dHdT3DlvOWQVIaotUXuJKtNWv3EVIy5+luGfa0h6lTLG4kU8fU3I56V8rY6LWyNaexNg4jHOXsVFRjbNOo8sE3uW5ts218m2L44jD/7j/wCJo/Jljl7GrTVrwqKT8VKKS/pZ30alz0q1iIcVrTMvNKXya4hb8RR90Z/gSx+Tete7xNP+SX4npOYfMWyPxGy86o/J1Vi7/OoclTl/cXI9ApccTH/Kf9x3GYWYznjrM7MJjktHUuKXQN/+T/tf/QdXoIpb8S1ypL+47HMNnJrSK9QTe09y4lfJ1DjiZf5UV/yJcN0ApwkpfOKjs72yRR2GcZ1C+K7KnS2VGKSzydvBB/8ATo/Xl6Fh1BnUGCFbPh9aXp+A7wEO+XmvwDdQB1CcDfMaffLzX4DfNKf2vMTqAuoMNF81p9z8xvm9PufmyJ1AHUGGpnSh3erI5wpr6PqyKVQgqVCcNRYirFSStZN2947KGO1Rbw9TNCMu9a8+JW0YmJPUIcI9WTVNxBhN7ISvNkUmE2RyCEdxDCA8c2dLgalF6mRhNJGlTZ5/JGWdlJ9FjJddAp6gYiXWQTe46KT6c3L8k83ZMjpsaq9AKUinP7hfhXoyMvFyTmXcxk4mV5My4fk15OnV/J1i/Z4yUL9WrBxt9qPWX3nrVOZ4PsbEewrUqn1Jxk+V9fS57jRndJrc9VyPQrOw5bepXVIfMQxY9yyqXMNmI7iuAbkNmI3IWYkG5A5wGxmwakcwXIjcgXIISOQDkA5AuQBuYLmA5AORINyAlIZsBsAnIhnIeUiOTArYhA7Ln24dzzLk9/68QqqKdKeSpF8G8r5P87ETHohr1NxXwu9k9TcyDC8TJdZbBkx2NJBCIQhE6PHKULSLsJGdg6ly7BnByfJ116RYmWqGlXVkR42RUzam1PixvG2aNTEXVg6MtDPzFrDy0K8vuFuOMlbcihNLPqXLlbEU12r2K8PbS/R6kHvR7D0VxntcJQm+17NQl+9Hqv4HjEcXwPRvk2x+enVpX1pzU0vszVvjH1Ovin3jnvHrXfJj3I4sK5syFca4LYzYBNjZgWwXIAnIZsFsZsAmwGxmwWwHbBbGbGbJCbGbGbBbAdsjchNgNgJsGTHbAbAjqGfiol+bKmIWgF+jVz04y4uOvNaMbCreUtl1NKlPu665PR/rxL+E3MymPa2rADCYzIFab1YgprViLYPGsHDKi2mRLRD3PPtOy649K2NepUTLGLZUUtTanxY2+SVtlvCvQpXLmEehF+lqz7XIkGLpZkTwCZjS3jOtbRsMmFNHWdAsR7LGRj9GrCVN8+1H4epzdWGV34MsYDFunUp1F9CcJ+Tud1J965rR9PdYSDTKuHqKUYyW6STXJq6LCZsyE2NcZsFsB2xrjNgtgO2M2M2C2MDtgtiuM2SE2Z2L2hKFejRjDM6l5Tlr1YLiir0nx06NFOm8spzUM63xVm2146WOSe1cTKOWVabXfdZrd2bfbwuRMj0VgNmF0TxM6lOpmqSnlmoqMnmy9W+966/cbTZITYzYmwGwE2C2JsCTAabK9UlkyKoBRhUyVIy4Xyvk9Ddwq0ZgYmN0zb2VUz04y42s+a0ZW0eyFsFhsFoqsqzlqxAVpdZiJQ8mUB1TfcTRiHFanma7Wbi8JU35dCr82nfcdHineKKHE3i2RjO1dnWZ82n9VlrDUZLei85aBRZF7ek1rkoowl3B5JdxPFj3Zz61U5wfFEEoT3KOhotMSizat8jFLViXo/RDFOpg6TfahH2cucNPhY30ziegmIt7Wm++NSPwl9x2aZ30nyrEuS0ZMwNsZsa4LZZU7YNxmxNkhNibBbGbAdsG4zYLYFTa+D9vSnTVszV4N7lNbvw955HtDHzhOdNxcJQk4yT0cWt6PZbmNtXo3hMVUjUqwbnG13CTh7RLcp23/ERn2iYed9GNr16OIgqfX9tOEJwldxknKyfg1d6nrcmZ+F2PhKUlOnh6cJpWUoxs1+fiXmybTH0isT9mbBbE2C2QsUmA2JsBsBpMjqBSZHJgVqxPsLEqLqU5O12px+DXwIahQnLLNS8deTIt7hEduseIh9ZDSxEPrIxHcCd7GXk18V+tiIZnqhGFOTuxFPNPi5JMKMhkkJHE6k9V9Uptalqp2Sq0axikisPDkMFEi3RCRMJDBxMVzWHQ9h0IS1OjGI9niqd9FO9N/wAS09Uj0aMjyinNxlGS3xakuad0eoYaqpwjNbpxjJcmrnf/ADW2sx+Obmrk6sXGbGuM2dLA7YNwWxnIB2wXIG4zYDtjNjXGbAe4LYzYLYBNgtguQLkAUpEbYmwGwHbAbE2C2ApMikx2wJMIR1GUsTG6LsmVqyAkw1TNBd60fNEkpFHBTtKUe/Vc0W5M5rxktqzsKNXtPmIactWOY61cqpv9IdT8ELOh8yMcaDnPQruZPKasQuaLIk6mFGXgMpoJTImCEifgEpeAKqCzlMW0akh1JdwHtEFnRGJ0+ZHe9FsTnw0Fxg5U3yTuvRo4HOjp+hmJ61Sm/pRjOPNaP4o6f5py2frHmjauxuDJjJgtne5SbBbE2C2A7YLYzYLYBNg5gXIZyAJsFyAcgWwCbBbBbBcgCcgXIFyAcgCcgHIFyBcwCcgJMFyI5TAKTIZseUyKUgrKtOWWSkuD15cTQlPQz6yumT4ablBd8eq/d+Rjyx9teOfpBOpq9whpx1e4Ry7DpctYKxMku9eYStwaKJV5RA9my8nfTRvw1I219b1ROyKvs2W6eAqNJ6WfexuTRnV9ozpO8brvy9l+4vXxmf8ApW3lHTZhs2XGaXJNksdmR4zb5JIyMD0spSeWosrva+5ev4nQYfFU6msJKXG19fI6I46fjCb2Rx2dT+0+ciWOEpr6C9+pYRBUxdGN1KtTi02ms6eq3rS+paKVjqFZtb9SxpxW6KXJIt7OqZKtOXDNlfKWn3mW9pULXU5zS3uFOo1Hm2kiOe2KNnlU5uztbTrW03J8S0ekPRrjNkGCrqpThNftIRl5olky6DSYDkDKRG5ASOYLkRuQDmBK5AuRG5guYEjkC5kTmA5gSuYLmRSmRuYQmlMGUyvKYDqA1O5kcpkMpletioQ1nOEF9qSj8SNFtzI5TMett/Cx/bQl/wCu8/6UzNr9LsNHd7SfujD+ppjTJdPKoRSqHGV+msfoUk/3pu/ko29SLDdLJTlabjTjwUad35uTI8k+MuznMkwCbc+7TzMjA4mjUterOV+Dbp/BHR4enFRSirJLTezHk5ImMhpSk7qtOk7vWXkIt+w8GI5PJ0Y4KVbLrOcF4NfmVMTtqlHspN+9I5OpiZy3yY0KMpPSLZ3eFY+nL52zNdphOl01TyQhGE8zWeMFFyjbRt/iaTrqpCFSTjOpLtyjdda2l/E5HAbEnON5rLZ6Znw5HQ4eKp01Ti3dO+7elG3xuZ8tdiJa8Ns2P2FzOvq+pHOnCSs4IjWYNKZj4tNZWO2NGWqg+aZlKlXoO9NysuD1X5e46tZ/0iGvhs29a95es2jpSYie1DAdK5x6tVNfvXa89699zeobSw9Rb8jl9NNLymvvOaxuy3Z9W68DJeHqU3eDlHw7/dxNa3ie/TO1Px3eIwVRpOFV1I/Vbim+UrNPyRnVll7cZU0t8qlSajbvuuq/MwcFt6pTdpZo+Mey+cWdDhekUJpKaU1J2vDhpfrRe7cXUyYegdDcUqmEjFNP2c5Qur2t2la/P0NiczkOieOo06koQajCqlpqlGa3aPRXTa8jqK0zRU0pkbmQzqpb2lzaRnYnbeFpvLOvTU0r5E80rfuxuwa1XUI5TOaxPTPCQ3OpP+DJ/W4mTivlAprsUrv7dT+yLXqRsHuXcuYDqHmmI6e4h9iNOC8IOT85S+4zq3SnHVL2q1NeEXGKX8sU/UibQtFJesyn5FHE7Ww9P/Er0oeEpwT8rnk9SWLq9rPK/wBdyn/U2FDZOJfHIu7M4+iRWeSsfaY47PRcR0qwkP2kp/uU6jXna3qZeI6c0FfJTnLxlOEV/pcn6HKw6PSb689eOjb82W6fRymt85S9DOeev6vHDK7W6dzfYpwj49ep6dUo1+lmJnuquP7lOEfjmfqW6ewqS3Rvzuy5S2VbsxS9y/ArPP8AkLxwx9ucnjsXV/aVWvH2lvK9vQjjsyvLW9r8cqi/NanYw2VPvt5fgHDZEuMyk8tp6haOOsOPWwZvt1Pv+LLFLYFPjKT96XwOvhshLfK5JDZcF48x5XlPjWHMU9jUF9G/O7LdLZ0F2VblGx0KwMVwQ7wniiMme5NzpjwwvjLTwOiwFR5EnCcsqSTWTd43kVY4bxNHC0LLhqRMRCZ2TZ/sT84/3CJsi7hGWx+GS80wnR5Ltvmka1HBwh2YrnvZchBvcm+SLVPZ1SXBR5/gd08la9y56cV79VmWXOL/AFoDTg78oo6CGyI/Tk3y0RQnQjGpUUdEpJLjwMZ5q29Q3n+W/HEWt6V4wZJGmyzCKJ4RXiV0UlSfeNKk+807L9WHyk6qyZUH3lXE7OUlvV/E33FDOC/SGmOFxuyZK+5mPPBVIO8bxa7ro9NqU0+7yTKtTAJ7rcrIReY6TNIntxmyukOKwtSM4RhOUHdKcG0/B5Wi/tLpptHEyclGnSv9GhSlGPlJs6Cls2Lkk9E3q8qZaqbHWZqCk48HJJX8jSOa2eoU/wAddcFOvj6naqVf5sq/0i/6ViZ9up/NUlJnoFPZD7kvcTR2Mnv+BHnyT9JilYeeQ2C+M17kWqexYLer83+R6DDY9NfRT9xYhs6mt0I+Qy89yttY+nB0NlRW6nH3pv7i/T2dU0tGK91vuOzhhI8Ix8iRYZLgiP8AFvcnm5GGyqj3tepZjsmXFr3I6dUEP7FExxVR5y5+nsrvZOtnpcEbHs0M6aLxSv4r5SzY4W3d5Eio8i9kB9mT4wnZVlT8EC4PuRacAMow1WyPuQziyeUQJJA1BK/gRtvw8yWaRDJIrKYSUk2+GhpUr+C9xQoRNLCJvhc57LalyfqwjQVLw9RHPidQ0ejkV+0a5QiixDo/T41Jv+VfcIR6/wDrcf4yn+zm/U0dgUeLm/4vyPPcckq1VLcpyir+Gg4jDm4q0tHjC9Oa/JWfKd9whjMmjMcRSIhEiUx1MQi2QgnMXtHuEIiYhIJTYoyYhEREImZX9m0889d0Vf3m2qS8BCNqxGM5mdJUkEqaEIkEqaFkQhEpOoDZUIQDtAtIQiEyFjZRxAC4oBoQiqQNAzSGEEI5kU0IQlKvNgwjdjCM7LQuU4pGrgKd2rCEYX6S3YUXZbhCEYpf/9k=",
              title: "សេវាកម្មជួសជួលកុំព្យូទ័រ",
              desc: "ជួសជួលកុំព្យូទ័រពេញចិត្ដណាស់",
            },
            {
              image:
                "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBUVFBcUFRQYGBcaGhobGhoaFxobFxcaGxsYGxoXFxsbJCwkGyApHhwaJTYlKS4wMzMzGiI5PjkxPSwyMzABCwsLEA4QHhISHjIpJCowMjIyMjIyMjIyMjIyMjIyMzIyMzIyMjIyMjIyNTIyMjIyMjIyMjI0MjIyMjIyMjIyMv/AABEIALcBEwMBIgACEQEDEQH/xAAbAAABBQEBAAAAAAAAAAAAAAAAAQIDBQYEB//EAEcQAAECAwQHBAYGCQMEAwAAAAEC8AADEQQSITEFQVFhcYGhBiKRsRMycsHR4SNCUoKy8RQzNWJzkqKz0gdTwiQlNEMWg/L/xAAbAQABBQEBAAAAAAAAAAAAAAABAAIDBAUGB//EAC8RAAIBAwMCBQQCAgMBAAAAAAABAgMEEQUhMRJBEyJRYXEGMoGRFLFC0aHB8DP/2gAMAwEAAhEDEQA/AK4+6C6ats76D83474ckNvpG4cUIG38XEPm3kl3B8WzCgNvyggEq8G9kCc29vXfDsm34lW37qIGRKF8vk6VCOLZdaOCcHv8Am6wXdzYZhAyINjb3FUjBt84Sjwb5woS2/chMWjb8gUbfmUo2/e5t/IjQ1tvhRQS3xdajb+BK5Pa3UIQuLbw3QtHhv+bxhtW35Q7W98IAqa1xzbdIG38SlcW2NVIcl57/AHvYgMUGj4NgBS+vz674Sr8Hz8VUW3huwIBQH4fLpuqgGtv4cwHJt8YAIQBBSuBbeqBJo2/GFAbfnCgNvdqCCBbfkIA349d8KDng8d78SUL6Pw3UQACX4fLpuBaBhmG2IeRUN7eu+GpHm374QQSG3zwgqa7uLfKqhLe50zRQb4PGiEChXFt7aBDb6VKYZtvKoE5tviCAQUo2+YQgZvyfSHXW/h8IjUBlsbZKHIbdD/8A0Htzghr1/wCJezKCAP3OcVbfhACW3XZmmLbr4rjT5t130YSjgS3xdaK/c3VtC2/CHBDb8awRrwBXgcW3rFj2UsQnzJili8hFEgVOKqVVe4Ap43jzq7SoJSTub/Iajs9Z5kqwFctFZqkKmAEYqWoFSQeAujlFe5m0kl3NLTqMZycpLZF7N0FIV/67u9JKegw6RWWjskn/ANcwjcoA9RTyjJdh5lrXbR9JNMuilTL5UQRdVgoKOBv3aVAUNWF4D1OKMa01wzYqWlKXMUYe09m56cUgL9lWOWOCqa3jFdPscxHry1JyxKSB4vyj0qEIieN3JcrJSqaXSl9ra/5PLwG35woHFvpyj0G0aJkL9aWmu0C6fFNDFerspLWaIWpNdRooe49YmjdxfKwUqml1V9rTMarKrb3xVWrSpC/RoACsCSRUUrTdi9wubfIuLXLvA3VFNRroTiOpZpj7R/5C9yR+JvM16jjDMWNsKEZ1ema47fks5+l0oVdXNAOdLuNDrwB1RGe0Er/fH8h/xihtFuAkrlBPfWqWVqwvqQmUkIQk7AqpI193ZFPOpqJOJpX1ruqtMoz/AB5+rOilZ0F/gv0b6yaWTMVdROClUJpQA4Z0qMaYnxO2O1K1/bPgn4OgjKaGXJNukmQlaUUVULUFG9cXeoRmNUaa0ruoUrYCYSrzfdhVlQaz0L9HUip+vluGe964lShX2/6RD9FSP+glTDiszF1Os1APuhUiArib/wAmOen0FzBfo7bBY70uasrJKAkpwAFSqhrHMTg2/G10b+qn+yj8YioSdbb3i/aTlOL6nk53WaFOlNdCS27CCu9tmFutv3tCnyf5QqFEm6kEnYASfAY5usW84MhRbeEODb9wW82/faWLs5aJmaRLT+/n/KMYvrH2TlJoZhVNO/up8BnzMQTuoR75+C9S0ytU5WF7mOlJUs3UJUo7Egnxo+lLX/45aCm96Om68m94VjcyJCEC6hKUjYkADpE0VZXsm/KkalPRaaXnk2/bY8smylJUUkFKhmDgQYjUhvk6V2fbCwAoE4DFJur3pOR5HzjHFLfPrvi7RqeJHJi3du7eq4PjlP2GpG9t50Tm29UO29Ony6boSnBt4mJiqNI3tvWWqwLb5BxGurbrgGKVvbe1DkQ4bT4/OCFJ3+fwhIaSbkBEAS28PB2LLfOi0b5Q0kyIkNvpV4G/q30ap4va84cVNvDdgQM550j0q5cofXWEmhxu5rPJIJ5ePpNsVMl2dapKAqYE91OonDVroKmmukYnspZvSWsq1S0eCl1AI+6lY+9zOt7S2C0zUIRZpvo1A4m8pJphRQKQb1Me6cDXdGfcTzL4OhsKfRSXudegZs5clCp6Aiaa3kgU1mhIqaGlMIsoYgUAqammJ274fFYvhBBCQhCEx0WZQSlcw5JST7/dHMY5u01o9HYyNcwhPI4n+kGCo9TS9Rk5KEJSfZNnnlqXfUpRzUSo8VEkv8oyk4fTTTsQPxxqVpwbezPMTB37QrYlA/qrF272gY2k5lWb/wDclXabLL9EtZJMwzFISmoCUIly0KUtQzJJUAkZd1WcU01FNVMSDQ1FRsiytdkmUmzaUlCaUVJwK6VupGZITiaZA45xWLrhjhqplGWuDpJclv2UTS2SsR9Y4Gv1FxrtJH6NfsnyjIdkv/Mlf/Z+Bca7SR+jX7J8oI+PH5NBoz9mSv4h/DEKBE+jP2ZK/iH8MQJhkOCeXJbaP/VTvZl/3ExQWm1hAqeAFMSTqfyi+sB+jnezL/uJjF6bm3Fyln1EzEFXAEYvrnGhaz6YSZzuq0VUr04vvk3ej9Cy0ShPtky6ml6hVdSkHIrVmTiMto1xo9A2qyTEFVlVLUkGhuZg0yVrGG2KLtPoqZbbIhMopvfRrAVW4Sj6qqajU6swI5v9OuzE6yGbMnFKSsJSEJN6gSSbyjlWpwG87aRWnVlPll6lbU6SxGKX9m6QoEAjIw6GSydcOMRk4sLWG1grBEJNlBaVIUKhQpHmdps5QtSDWqVEY7tfl03R6cDGS7ZWWi0TRkruniMRlz6RctKnTLp9TI1eh10utcr+mZcpb5utHUD5Pw3Am17H4aqQlNrfx301DmBpS2+piGYN7b2TNv4RDMbfmSh0SCgdPjBC4bX/ACl7MoWGk5Fdb4dN0KUtvpVA29W6FHFvz3w0I4pbee+kczAVbfF7b8o5bfUpuJPeWQhPtKISOpetSeFkfTi5yUV3Nj2CstJJmHOYorx+z6qeVEg/ejRL0ckzUziTeSMBROxSc6XqUUcK0rjCaKswly0oSKBKQkcAKDpHaTGRLzPc6iC6YpIWEKo5LdpCXKTemLCRv17gMyeEYnTHbBa6pki4n7Z9c8Bknz4RHOpGC3Zo2en17uWKcdu7fCNbpXTsqQO+qqtSE4qPLVxNIzo7d97GR3P4neptpSnKvOMWpRJJJJJzJNSeJOcEUp3Tb8ux11t9N28IYqZlL1zjHwex6PtSJyETJZqleW3YQRqIOEU3b60VXLlDJCbx54Dy68IoOwmnBJnJlLP0cxQANcELOCTwUaJO+m+OntPPv2mYdlB4DflnTlxjTspeLJS9DhPqO2dipUuzez9UUc1JfNs0zl0lVpAz7gHSNLNGDb24Rm7EfpZw3y/NXwi3e/YY2i//AFfwUFutalp9GQaJmTFpw/3Ll4Kpr7iesV6kmgFDtrQx6KZQ2CF9CnYIyzpXDJlux1kWq0BYBuoStSiRgAUKSBxJIFI0ukv1S/ZPlHXIWUhSUmgVS8BrpiKxzaT/AFa/ZPlCDhJYNBo/9myhrC68lIBHSIERNo39myTtWB/LLAERoENjwSSLPR/6ud7KP7iIyumbMFoUNoLfyjW6NH0c72E/3ERSTgKH4N+J0rNZjJfBzWtTcKlOSLv/AE30sZtl9GTWYiqTtqmg8rp5mMf2rXbjbVXPT3749D6O/dKMLt2mGWe+tYk7KWz9Ft5T9SYK8054eyT0j2NCBmNcVKkXGTRqUZqcFILNeuJv0v3U3qZXqC9TdWsSKMNjntdoCEkk5QwkIdJ6UlyJapkxQSlIqTGOsvbudNVfRZ0JlE92/MImLG0UFB4GMx2k0z+mzVC9SzSjVRrQTFDVwHxjPI0nZpqpptSZtAgfo/ozQJWK4qFczhTMChG+ChYye8aK0vLnju1Cx60tXrp37xvGET6UsgmyloOdKp3HbHiXZrTy76Ja1G+P1cwHvpIFaE6xQa88jWPVtBdpQsplzqJWcErGEuZu/cVuOB1bIK2eUMnFSTi+GZFSKEilDkeNcQ/OsMuNvpW27RWH0U9YFbqu8nZjmORry3RVFDe91y24SUoprucVWpunUlF9nga2/eREstvoIlWMG3wrCpLb8w8ZEgq2oPbnCw2iWT8IIaT5GVNW9fWHUO0eLfOEBb4vGoG3ltEAIUfg2AZtCWf0tslpzCAqYdmHdT1UD93wgVhjg28K3nYOz19LOP1l3E+ygZj7ylD7vKILiWI49S9YU+qpn0NopYQmpIAA1xk9MdsUpqiQL5+2fUHDWroOMcXbfSK7wlAkJIJI+1TIHd8I86sFrWueElVEm9UUwAAJHjQeMY9Wo1lLsd1ptpQfRUrptSeEl7d2aW1LnTPppl5QrdvEG6DsGocBHJE6rYso9HeNytbuqu35RBGZJtvJ39CHhx6cJJcJegQQQQ0sCiLWxWkzAbx749Y61VxC+ePMGKmJJE24q9uod499MDyO2L9jc+FPfh8nK/VOkfz7RyivPHLXv7FvOS2+lc3YT9JO9tHkY0cwgprXCla4HBvMjLWI/TzPaR0BjZvftR5joq6arT9DQJMPSIiQYlSYzTpyRKY5dKJ+iX7J8o60GObSp+iX7J8oA1l9owf9ske2fwiI0JiXRf7Mke2fwiGJhseCSRZ6LHcm/wAMf3ERSqO5t6heaK9Wb/D/AOaIoy2/OunY9/wcvr33Q/Jn9Py1JuzkDvIUFDgMxzx67yfWey+kROs6Fg1wA5UqnphyMedW2VeQRufu6bo6P9MdKGXNmWVZyPdrsOI8DUfeht3T36kP0mvmDg+x6jOmBIqY8z7e6eXMWLHJPeWAZhBxQg5A7CRjwIi27cdpxZ0hCKLmqNEoOIJ2qGwRitG2YovTJiiqbMJUtRzJOJimlk2CVejE+g9CnDDPfQg14gkc4xVssCpaymZVOBIIF4EjKmWBNMdVeUb6/CLQlQopIUNhAPnDsATKTsto4gCesGtKSwdhFDMPIkDiTsMadKtRxBzByMQJwyyhyTBSwE752kJhQlCiZiEeqTUzJadaa5rTkdophXABiVgioIIOsN+cAVDPRm9eRQKOYyQvjTI7/OLVCv0eV8GPf6d4z64c+nqdCqUbfGkawKHFt5AiaEbDkQcwRmC9mqlULb/x0U01lHPOLi8PlHMV8esEFBuhYBLsMJ10bq6mFfyfwENBbeG6Cjb6VASC3TQlBJOQPxbMehdmLD6Kzy0EUISL3tHFf9RMefyZHpJ8qXmFKBPso75B3G7TmOA9WkIokCKNzLzYNzTqeIOXqZDtrowrR6RI7yMeT84wyUjMAVOumPOPZLXJCkkHXHlWmLCZM5SNRxTGVd08rqR2/wBOXqhU8CfD3XszjgggjPO7CCCCAEIAYIIIjss0wXSnZlw1jkaciM6UihsB/wCoWP3h0EWss4vN+cVOjQf0pYIobxw5CNaFx4lFRfK/o811PSP4d+6sF5Z7r2fdGjSIemESIeEw0gY9Mc2lf1S/ZPlHWgRy6VH0S/ZPlCGsv9F/suz+2fwiI0RNosf9rs/tH8IiJAgQ4HyLXRHqzv4f/NEUN47G3WkX+ivVnfwj+NEZqaug5bqOnLlGlY9/wczrqzKH5IrTMoKt/PfGTXpEyrSmcg+oDe4audaUG7UI7NN6QuigOJ+TdIqtF2T0qgo19Gk1x+urbwGQ+ZhXVRPyoOmWzj52W2j5cybMNpnmq1eqD9UbotyYiBphDgYpmyPSYlKDQHbkPeYhETldQNoFOIzgjRJK61BwIwIiZMQSUGpUcz0AwAidMIcSw6UipxNAMSdQGswiEVNBFpYdCKtN+WMJSAb6v9yZSqZI3ZFXIa8EMbwslVKm3yuYMAtVUg53QlKUk8btee3IUG375lJo+VH8zEsBt/h2YRUYpI4yrUdSpKT5bOdvCCHUeEEOEQAkvo/cBCE5tvjDiG30rHOVQHF/m9jCRbss+xtn9JaZkymCEhA2VUbyuYCU/wA0eipjJ9g7Nds4Wc1qUs8Dgn+gJjWCMurLqk2dPQh0QSBQjJ9sdE+kl30jvoxHvEayILRLqCIjwmsMnjOUJKcdmt0eOAwsWnaPR/oZpIHdViNxfuirjJqwcJYPT9NvFd0I1Fzw/kIBBCCGRWXglvK0qNGU4rLSFMEAgh04dLK+mXkrql1SWGn+wiaRYr0xE5OaapXvqO4roRy3RDHXo60BCu96qsFbhqNNxod9CNcKnPpkS6hbKvRaxut0dikwAR02mUUkg6nhtEQARfTyjgasHGTTHIEc+lk/RLp9k+UdaBD1yrySMqw4iZ36InJVo2QEqBIWQaGuISKwIEceirJ6GV6K8VJCipNQO6VAXss60HhFggQIrCDJ5O2wmkuf/BV+NEYbStrCAcX+brhGz9Jdk2k7JCvxy48h0jalTFXE440G/wCT426FXoi/wZV9b+LOHoskaEKnzKDLWdgjWWeUEJCUigEcejbGJaKfWOJO0x2gxE228snjFRWEPBh6YjSYkTCHjwIkSIYmJEwhEqRD0wwGOizp+sQTiAAM1KOASneTBEd2j7GuYtMuX+sXr1S0D1ph4ZAayRHo1gsaJMtMuWKJSMNpOZUo6yTUk74rezei/QoKl0M1dCsjIU9WWn91PU1OuLoQ1sCPPu0tj9HPUad1feTsxz6+Y5Uc2jb89/2vsl+V6QDFBr90+t8eUYFan+T6U1bap1wXtscpqFDwq7xw90c9WxBAS8YIsFUiq28eBjjt5JTcT6yyEp4qISOpeUdp49Wzzg0LI9JbJY1IClnZh3U9VA8uNYKsumLZctYddRI9F0VZxLlpQkUCUgDgBQR3RHLFBD4zGdIh0NMPRTXBdhZDgzvabRgmyyKYjEcY83oQSDmMDHskxFRHm/azR3opnpAO6rPcX74r3NLrj1LlG5oF/wDxq/hyfllt8P1KOCCCMw9ECCCCEBJJYQQsJCwhxeaPnekl3T6yABxTkDyNE8CnYYQpxiqsdpMtYWBWmY1KBwUk8RURfz0DBSTVJAIO0HEE79u8Eaot0J52OP1qz6JeJFbM5Uzk7ehjoRPRt6GGIljYPAROiUn7I8BFo51k0lYVlqidMRy0AZADhHHpfSaJCFLUdXidQhAODtVplMqROl1782WEJAzxmS1K4d1KoyuhbBdHpFDvHLcIgs6V2iYZ8zKvdGzZFyDDkiCcsseDDxEYMPTBGkiYkSYjTEiYIiVMSpEQoMTphAJZSCo0YjXdlNF3qWhY7or6IHwM07zkN2OuKnQWifTLuH9Wmhmn7WsShxzO7jHoKU0FAKAZbBwgNhJZUSxFKiWAIZMQFApIqFAgiPK7dJ9GtaD9VRGrVX59d8elaQ0lKlC8tQrqTrJ4R5pa56lrVMOaiSeery6bovWSllvsYesSg1Fd/wDo5DXdBBUwRoZMQgIDb36rfsLJClzpn7yUDgkXupUfCKYFt476QyxTZ1nWpcmZdKjVSVC8hWrEZ1w1Y568IrVouUcI0bKpGnPMj1YQsYay9t1pwnWev70tX/FXxi7sPayyTMPShB+zMFw/1YHkYoShKPKNyNaE/tZfwCI5cwKFQQRtBqPEQ8GGEgpio05o8TZaknZhxi3htyppByL4PG1y1IUUKBBSaY9IZFr/AKgWhEq3BA+smpOzJCfG4ekVcZden0S9j0jRr7+TQWX5ls/9iwQkEVzZFght6HQhJ5CLjQc+v0R3lPgVEdCR97bFPE9gXdmoVWlFJNTkACKk7odBtSyitd0o1aUov0L5Usg0OqJUI3mFnzkKKVJIAKEGlcgUggeFIRKxtHjGnHdHnVVKM2kSLXQVjzjTFrVarRcr9Gk5e+PRJlFAio8Y850rYF2aaZicUE7ehg9yCb2LJKQkADIRKkxy2a1JmCqTxGsR0ph5CPBjukyQUXqE4Ekg+rTVTWdeYivSYmTBESpMPERiHJhCJUmLTR1kXMWlCB9Iv1a5JA9aYrcOpoNcVssgArVglOJ37AN8bLspbbPKlmZMmfSr9YXTVCR6stPCuO0nhDlFvhEU6sIfc0vk1WjLAiTLTLRkMyc1KPrKVvJjrUQBUkAbTGVtvas4iVL+8v4B9aUFrtsyZiuYTuBoPAPpWWFpOW72KFbVaNPaO79uDaW7tJJlYA31bB79kZy3dpZ8z1SJY2DFXi/OlJdoG/z40DRt+d2naQjzuZNfU61TZPC9hVqJxKqnaTVvcDETni+fN1IcQ/Fs0Ypt+QNjGChnLyyKp2vwghbvGEhDzn3N/HVWkBLbw5Q2rbx3w1vw6cojJ8EmBbe7KNUhJzALq/zMPSdb2+TpiFvvFvbiULLXBDKkKlmsuYuWf3FFPQYF7q2tm7S2yXgVImj99NFfzIp1B90cTfj13wGjb8REcqUZcomhd1YcM0ll7cS8psmZLO1NFp6UPQxf6M7Q2RZBFoljX3jdIpjjepSPO1AYtvdRtwA1pEUrWL4Zbjqclykyq7azP0idNnY0UrujYlNEjhWlabTtpHBoa1303Ce8nqIurZZ7wyeUZW0IVJmBYGFceDeoVby2zDY3fp/VnQrpyez5/P8Ao0UENlrBAIyIqIUxgNHrMZqSyhYWGLUBiT8TuA1x0SLItedUJ/rP+I655UiejbTrPEV+TM1LWbXT4dVaW/ZLdv8ABCVY3QCpX2RieJ2DeY7Jei1HGafuJOH3j9bI7uNDHdZbMmWKJFBmdpO0muJpr92MdFG+ToDu22nQp7y3Z5hrH1ddXuYUvJD25fyyvOiZf2U+A4v4Yw1Oi5X2R4N+EWODb8i6G31jQ6I+hy38ip6sq16NlU9QNvVW2zRopRIpXYT7n5nSFOGqjfyrWNcure7puhsqUWuCWndTi85Z55OkLlqvAkb/AI7YtbBpEL7qsFdDwi8ttgCtX5+edeuusZa36LKDeS28MYoVaDjujetb2NRYfJeUiZBjP2HShHcmfzf5RfSiCKiKxeJwYfKTU0iEGOmTKvm4DhgVkbDkgHadewcofCDm8Ijq1VSi5S4R0WWWFkL+on1B9o61nyHjsjvSkBve8QqQAKAUAwA2aqP5w6oo29mB1qdNQjhHI3NxKtNyY2rwb3YuUcG3vwaVNvpRCW31rIVwJbfvQkNvyVRbfShUNvxJQhrb+bFUbfk9t/ANbfxKHIj9GNvVMEPx2vwggYH9TK6rYfMiEOxvDpuhghyatum6Ii1geG+brWHa23tGMYybeyohwLb6GCBolep62DSAN8nSI78FYQwkZbzrljDwdfub35xJGNW8+u+HAh8Pg6ZETQKAdHtdRFfpDRwWKU+bwixfxfyJWrb9waTWGOhUlB5iZiySpsruFJWjURSo+UWEuVMXgEXR9pVMOCQcedIt/fxxZdcS8F+XvdRFF6fScup/o6Kn9V31Oj4MWvRPujms1hSg1reV9onEbhqA4D3g94wDbG6IgaeUK/dhyeqL8IqKxFYOcr1qleTlUk23y3ySb283WBvHYeuzGI64NvmFBbeO3EuIMElcObf5EObb5BlX02PpAnHNt7QQYH3m3v8ArQUzb1/LIoDlz8W9qjwbeMIQ1SG+TpHNaLKFYPy29d+EdpbfL6zbofwfugPDHRm47mU0noUHFOBZbIq7LMmyTQpKkbNY4fCN4Ut+LrERsya1o35+NadtGTytjSoanKCxJZKWxWgzD3EKrtUkhKN528BF/ZpAlpujHWSc1E5k01l7YelIGTeUOfubEPpUYw+SveX0rjC4S7DgrBt8ioVhgG3XJlW37gt83mZyhgcS2+tQlt9AG3scn4PZkmHEvFviYIsATubeZLLzYfgIWlMNTboRt9cQgoUNvzMOVR8W821fuf5m1t8AgBQb+vwght9swQsiwVNX083Q4IC3w6cDCwRAaOBxL6/P51qiW/GCCENY8N8IcDBBBI2PNHvwfDgYaXwpV/HNYIIsDrrwevryCnZrY83rgggjRUjrDy+eD/IgggjO4idr1/BnErqeyvli6QQQgvkRKa9fMe8jx8JUp1PMjzwdYIIQ2Q0aqDP31p74KPlWr88YIIQu5KB8n4/LWh10eFfJ6oSCCMQ0aufz8x469Qo5h7PPB1gggDhdVXn8unCFpi+NfA1+eEEEEAJD4UfOHHPdX30hYIQ1iAvi+nOFSrU2+JBBAPfT4PVCBT5h+HIghAQgOPh5t4wYZUbe0ghIQxZo+Bbo9Bwe8fF5kEEc+B9HX5QQQQBh/9k=",
              title: "សេវាកម្មជួសជួលកុំព្យូទ័រ",
              desc: "ជួសជួលកុំព្យូទ័រពេញចិត្ដណាស់",
            },
            {
              image:
                "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxISEhISEBIVFhAVFRUVFRgYFRIXFRUVFRUWFhUVFRgYHyggGBolHRUVITEhJSkrLi4uFx80OTQtOCgtLisBCgoKDg0OGhAQGy0lHx0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIALcBEwMBIgACEQEDEQH/xAAbAAEAAgMBAQAAAAAAAAAAAAAAAQUCAwQGB//EAEQQAAEDAQQHBQMICgEFAQAAAAEAAgMRBBIhMQVBUWFxgZEGEzKhsSJC0RQzQ1JTksHwBxUjYnKCorLC4dI0c5Oj8Rb/xAAaAQEAAwEBAQAAAAAAAAAAAAAAAQIDBAUG/8QAMxEAAgECAwQIBgIDAQAAAAAAAAECAxEEEiETMUGRBVFhcaGx0fAUIjJCgcHh8RUjUnL/2gAMAwEAAhEDEQA/APjqIiAIiIAiIgCIiAIiIAiIgCIiAIpKhAEREAREQBERAEREAREQBERAEREAREQBERAEREAREQBERAEUqEARSoQBFKhAEREARS3MLZcVkgYOzWKzkz5n1WKqCEREAREQBERAEREAREQBERAEREAREQBERAEREAREQBERAEUqEBKBQsla4JUOUIUbBCIiqDNmY4reVzszHFbC5WQMZczxPqViUkzPE+qxUMBERQAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIAiIgCIiAIiIApRQgMlBUIgCIiAkKSViilMGTvxKxUlQoAREQBERAEUqEAREQBERAEREAREQBERAEREAREQBERAEREAREQBEUICUUIgJRTGwmt1pNM6AmnGmS69G6MltDrsLC45k4BrRtc44Abyj0Bxrr0foya0OLYInyEZ3RgN7icGjeSvR2fRFks2M7vlEo9xpLYQf3neJ/KnNZW7tFI9txlGRDJjAGMHIZqC2Urpuy8kfz00DD9UPMjxuIaLv8AUucWGFubnv4AMH4lHzE5rEP3fnaEJsjd3cAyi6vJ9QtTo4j7g6kelFiFg91KACrjgAMyU1DSNobEPom/elP+a9HYOy0bm352MiZsJlvc/awO5btE6JZZI/lFrP7X3W53K5ADW8+Xmqi16TmtD6ta4geFrQTdHLXvXFt51pNUnaK3y/S9fb9FYWnQgpVlect0P3L0X9XDtBaOH0dpf/CWsH/scCtb9BaOOUFtbvvwO8r5VTMJ4xeex7W5VIotLdIv2lbwjFq6m3239ownUlF2lTiuzK/2zrtnZuzfRWmRh2TQSBv32AhVsnZm04mMMmaMzDIyTqAajou6PSjiCDiCKcta1NtTnuaKEyAgNI8d7IXTnVWyyS0lz19DPPTk/mhb/wAtrwd14FBNC5ho9padjgR6rWvuQ0QO6YJj3j7gv3qOaXU9qlRhjsVDbuxNlk8LTG4/VxHRcqxyTtNW7tUd0+iZWvTlfsen8HytSvXab7Cy2ehvBzTkbwB5ilfKi8+7R0oNO4eTuq7+0LrVaD3P9Hnzw1WG+L/GpworRuhrQcrM/nh6kLph0U8YSWUYa6zXj9ySnkirU39y5r1K/D1d+SXJ+hRIvQSaKjwNxzBWlLzicP4hgFTW2EMkewZA4cMx6rRO5m1bRmhERCAiIgCIiAIiIAi6dHWCSeRsUQq92+gAGJc46gBiSvUWXsnExodM58jz4WM9kFuouOJFc89ayrVoUo5p8TfD4apXllgtx41RfG0dV9FsHZN5xa1kLNvifT+J2PmvQaM0REw07yWVwzAJujjiGjnVcv8AkIcF799p3rompxlb8e/FI+NXhtWUTS43W4uOoYr7jaLBZCQZmMc4ZDB7v6A1oPVb7PbLPF81Z2D959K9AFD6Rgnr5u/Kxb/Dzf3eFlzbPBmzRQMaJnlrAAGwtIDztMmplczmcdSrLXplzm93C0Rwj3GDDi7W47zUr6c/S8RzjYT+7GwedK+SmLSANblljNMy5ocBxwAHRUp42jH5YJt/lt8kzWr0dWl805JJdlkubPjpa9xyJPAoYnDMEccF9dOnI7rnl1lDGG64sia8BxwDAWtIc+pHsDHHJcFo0paS8BsUbIzjWaIRSuG2OGO9IRni5rRlkuvaTtmyNLtaXm/M890Kall2qb7E35K3ifMmxEraLKd/Qr6Yy0Bjb0zWtcSaAEOcRqJww4VPFU9q7WRAkMNaZ3Q99ONwYLjWPnJ5Y07vvv5I7n0ZTjHNOrZPrVvN38Dxk9nLQKAknZj6L0nZvQzIB8pnH7WlWtJqWDaR9c7NSs4e2MTwGFobTO7eB4ua7Hmuy1W51YxCWGNwxfQuNdg1Dz1rLE4qpJZLZU9/W+zcdGDwNKD2qlna3dS7d7PI2y297amOtI/YNPgqMBSovCtcTSu5eh/Xzbt2ywSOAGAZGbo6DAclstuhoJsZIwXZhwAa4c2gKttmi7az/prXIWjJrnFrhwcMD5LHbYetli1a3W3bwT177d50bDE0HKS+bNrdL5vFrRcLXKy06MtM/tOgcZDrcZWhvAOcG+Smz9j5z43xs5lx6AU80f2l0jEO7mJOzvGBzuTziRzIXRYO1jhhO0EbW0B6HA+S7atTERX+rLbs92PPo0cLKX+5yvxv7udtj7GxAi/JI92xjQ31vL0Wj9BWWB15rAJdRJMkgO4DBp6KuZpaItvR2i4SNVK82uBHkqXSGnZYzVkrJW7KPjd0ae7P3VzU6lWt8sptPqadua/djsqUqNB5oU011p3l4/o9xNaiPCx3F1fQfFcEtsefeI4ez5jFeXsP6QZWVpUV1ObAR1bG13UrvH6TJnYPiiI2inofirT6Pq20l+El6pkQ6Vo31jbvv6NFgaZkVO04nzSpVRJ2uL8RHHQ4eHy8wrKzT3heNAM15NfDzp/UetRxEKv0s7IrLUVJoBmTkue1S2bLvRXcHEdQKKh0zphz3d2w4DH91o2upmTqCpjKK/PPBzwEdNWIBBwx2ld2H6Oc43engediulIU55Vrbq1/Z6O0wtzDgW7arxnaKzsD2yRvD2yVrRrm3XMu3h7WeDm48Vd2O3moZIQQ40a8ClTqa4ajs/DBYaesLHxOcBR7AXA7cqgjXgPILqwyeHqZZ8eJx4trF0c1Phrbjf8Ar+zyCIi9c8AIiIAiIgC6NH2J88jY4xVzugAzcdgC517XsLNEyN+P7Yn2q53R4abseqwxNV0qbkld+9X2I6sHQjXrKEnZcfRdp2dmNC3Ink4SOcYn11xtdV9BqBoxvC/tV461hri2Nt+TWfdH8R/BaX2oFpcD7OVR6ccfNVj31J9ohmpgNOuupOZXg1q08TLNNWsrJa+/fYfT0KEMLDLDW7u3p70WnHuLZ9or846+dgqGD8XcytUulCfZaCRsGDQqx1oZkXDhUINItGRCy2cnwbNXVS42LazxyvIbgCcgBU01mpyG/ILZaG2eIEumEsn1GVkNdhcAWjleXnnaVhBJLmXjmax1NNpzT/8AQxD32/1H0C6KVJR30835suSXmznq1ruyqqK7k3zbt4HQ/SNvfUQWeOJupzhfkpxfRnRiibQtokuie0lwaa3g594ggVDQ4XWDDCg1nNaGdqoRmSRuY4eZWyPtNCftK78BuyXRtsZFWpwUV2L19DlVHAyd6lRyfbL0PRsaGhjYyY44wRGxhLQ2vvV8RftdWpJKzdMyJjn0Gf3nHbtO9edk0w73QByx81wdo9KOMIFcTgOLsz0B6rlyYivUW1k3d8X+tx158NhqcnSilZcF7ZxaU0obQ9xLj3WIpj+0IzrTG5u181XnSBNWtNA0E0HhAAypl68VqtslyNjW4Gme/X0p5rlre9vDEFrhhi4g4034c178IRpxyx3HzFWrKpNynvO5ttMg9sXgMa0Ac2uFQQcVcdn9KGN1x+LHYjjqPlQrzcAI9gZkguOylachWp/0umQYYEVbjhljg4Dn6KtajGrBxkXw+IlRmpRPVz9qnCoa1oXDL2nmPvU4BUAvONTmtjYlxrB0Y8D0ZY6vPc9DttWlnyCj3Fw2HFcQdsC7bPomV/hYeeCsoezMh8TmjzTbUKStdIKjiK2tmyh7wqHOK9XF2Ub7zyeAC6m9lYdd481k+kKC/hG66OxD325ngXtWyzx4E4GvstrtObuAHqF6jtBoZkMYIDalwAOvIk+iooodQB2cBz1nM8gu2lXjOnnXceZXw0oVdk+q5NiiJc0DIZfHmvW2ybuoSTkGkngNSrNFWSmNB1/0su1UxEQGHtOY3zvH+1ebVe3rxhwuevRXw+HnPjYobXaDGwOdi5xDiNRJONd1BTptVYSWukNagg0JxqC5pBPL0XdpOXKrWkAaxjqAunMLW6EUvN8NLwGNWiuAOOOJ51XtWPnHv1MoZW4MI1UficCcbo3D1qvRRzX2e1mQWu3nInnnzXldGFt40q52B9oADxAVzNc9y6LY1xr7TgwNqQMtlT0Cyq0dqkuKOjDYjYtu17rcVr2FpIOYJB5KERdBxhERAEWVFLW8eiE2MaLdC+gqdR6VBFfRY3BvXRDZiRRo9p+DcQK47TgMQobJUbm+J4Ix4axQrU6T81K6ZdE2hpNWEAjHC8Mtd2vVc8lhlaASw0ORq2hANEsTmkR3+xYWiZjmgEe0D4sSabKE0WLoX08O7CpOPBdWj7EB7cgx91p9XfBVnJRVyadOVSVkaIbEaXu7kPNrfItJUOmYCaQiuPifIfJpartz5HA3Wk8lUDRsxziedeLTrzWdOu39TS/JvWwuS2zTf4NYteyOMcif7iVHfF3ipyDRnuA20XWzRUv2ZHBtfVbhoOYilx2O0YA6qnzWm1h/0ua9THY1bfS+T9CdHzkihzbhy1LTpmWrmDfX0W+KwSMeXSXWNxFXOaC7ZRufUKs0i+r8DUBc8FF1rxO2rOaw9pqz7TO3ykBuAINcCK1y2UIzOSzgiFaAAVLKZ5nPxGtdXVA4ujq3xDCusDds1rlgq1odte3+nE/3BdJwmdoA9pjTQ4VaaAl292R4YcFYzwAAUOdcOn+1X2Zt54Y+paHZ7gfCSdRpy4K4lLQ5rW0u+EUyJF5znDcXE03UUSdlctBXkkarJBeIBwbrcf8AS9Ro+CzR5EE7SvPtddGDHuG1rS7zGS1m3SnwRHmQPIrzK1CpW3XSPZo4mlQ32b5ntxbGAZii55dNRN114Cq8TILS7Og4kn0BWHyWXXK0c3/iAsodEx+5++RrPpp/bHn/AGevl7SD3Wk8aBcMvaSVxusFXHIAOceipoNHtPzkzyNjO7b5ucfRXtmtNmjYWBpjacyJIy5/8Ti4HkCtHgoQ+mDl36LxafgZrpGc9ZTUVzfgmvEq5Z5ZiDKcdTaD2eNNf5491isG0fFbm6UsrPAG86fg8rW/tS1vzb4wf+w53mFedCvJZYxUV3r9GdPE4aDzSk5N79Hrzt5FrHZnUwaV57tZ4Izq7z/EqLV2vtQ+bkB4RllOqorfpiacUldXGuQz2kpQwM4VFNtaEYnpKnUpSppPUztrhca66DqxJoNlRry1rW60ubWpN4ULsTro1zc9l3gVnYp2n2XgEGmeWC2nRhvPJdUODtWZJ1HIr0meQYaKa8vLiQWt1kA1Hjzz1DqFlpOU07sbr28jbzXdBGYmAE8Ac9vJtQDjStBqXBIw1JJb5KURluiu7sp3R2LsqdvkshXcp0GU4e7KldtXbP6QiaDKcwhO/oshCd/mu9jAM10Mprr0HxVLl8pWxwfnFdYexrQHRR0yqTJU87wpyXY2RutppwCxk7s5xk8mpcnJocJtNn1sI/gcP8mn1WLZ7PtmHAt+AW59nYco/wAPRYOsDfqHq5WzFNmw98GbbRM074yfNsn4LBttp4ZRhrMZcTvxCzbo9v1Xeaybo1mx355qsssvqRaKqQ1i7G9umsADOWEA4thDr51VvOF3kuWXTs5zndhsa1vouiDQYeaNDq76KJtChpoa4ZrPZUf+VyRrtcQ/ufNnA7SsxzmcebvisDb36zVd36rbv6qP1dHv6q2WmuCKudZ75PmVkk5OK1k1V03R0etrvvI6xRD3HdT8VZSS3GbhN6sqrNPdO7WrERxyAYjCtAXXSK8qHHGuHBdDLNEPo+v/ANW2OOIfQs5j4pdEqDRrknZGCbzamuDczXP2qClca0qTXMKkmthLr1aHVQ0oNy9PG5gyiiH8gW1ltLThcHBoTMS6bfE8u61zPzklcd73u/FZdzOfcmP8sh/BewZpaUZEfdHlVbP1xNq/tCh1UWWGkeMboy0H6Cb/AMcnqQsxoO0/YP8A6R6letOkpt3MBR8tm1u6AfFV2yLfCM81H2YtbvoacXM+K3N7IWr6rBxePwXom6RlGvyH4lR+uJxk/oxibVD4UpoexFrdqZ1d63aLqb+j2063sHOv4qxbpic++TxoofpKb7QjgAo2yLLCNnIz9HFoP0jfuj/ktg/RvNrnjH8o/wCa3M0hN9o7qVEukpQPnHdU26vYn4PS5lD+jw+9aBXc3/a7LP2Hc3AWlw4ABVL9LzfauHNywdpSX7V/UrTMY5Ei7PYSOtZJpjwkY0eca7YOx1gHiMp/nB63QvIG3v8Arnz+Kwdbn/XPmlxlR75nZrRrf9ySD/ILdHobRw9xh/ne71JXzZ1sd9c8sPRan2gnN7upS7Fj6h+r9HfZRdAi+WV/ePUolyDEPb9byUh7frlVF8qe8KmxXOi4vN2rJlzaqbvFIeosWU0XbRHt8z8Vn+y20/PFUYfvWxr96ixZS7C5uR7SsmNZt9PiqgSfvKRJvUWJzLqPS6PtAjcHDGhBpgNaxt8oke52IvEmmGFdS8/G7YVmZTt81TKaqppuLPum7VIDdvmVTmQ7VjXepylc/YXjWNUSRNOunJVLSVsLDtPJZvvN1K63HW+Jv1vJagxupy4nV48VgJCtF3mMmuotIhQg3sNhqtppeJrTdiqdkpqt7ZDVRIQeu4tmOA1raLUBtVMHn80WzvTuWDinvOqNRrcWptjdhQWtm/zVMbSRsWfy07GqVT93DrXevkWxtjNS1unByVWbX/D0CxNr4KVTKyr9fkWhmH5qodaBtHmq0Wvgjrdu8gpyaldrpvLBtpG1YS2gnWPNVvyo7fJDaTv+6rKCvcq6ratc67x2jqsS795cBnO1SZXbfRao5mzuawnX+eqy7gn8j4qvbO7b6LMzu/IUslNHWbOdZ/t+KxdZDtPVnxVc+ZygzFWRRtHd8iO139HxRV3fuRSVzRNKIikyCIiALKqxRATeUh6xRRYm7NzXnYpMhWlFGUtnNokUiTctCyqpykqZ0tlKyM5XJeU31TIi6rM3OmWBlWlFbKjN1GzaJFtZKuVSCjjclVGjtEp2qDOdq5byguVchfbPrN5lWBlWpQrZUU2jN3e/nFO9WqqmqnKhnZn3iOPFawVJclhmJ5lTfO0rBAhW5kXqe8WtEsRmZsEpUmdy1IlkMzMu8O1Zd6dq1opF2bO9KlakQXCIiEBERAEREAREQBERAEREAREQBERAEREAREQBERAEREAREQBERAEREAREQBERAEREB//Z",
              title: "សេវាកម្មជួសជួលកុំព្យូទ័រ",
              desc: "ជួសជួលកុំព្យូទ័រពេញចិត្ដណាស់",
            },
          ],
        };
      }
    
      state = {
        display: true,
        width: 600
      };

  render() {
    const settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1
      };
    return (
      <div style={{backgroundColor:"#F5F5F5"}}>
          {/* Search */}
    
        <div
          style={{
            backgroundImage: `url("https://thumbs.dreamstime.com/b/abstract-image-wireless-network-wifi-connection-technology-concept-bangkok-city-background-night-thailand-146823201.jpg")`,
            backgroundPosition: "center",
            backgroundSize: "cover",
            width: "100%",
            height: "300px",
          }}
        >
          <div
            style={{
              backgroundColor: "black",
              backgroundRepeat: "no-repeat",
              backgroundPosition: "center",
              backgroundSize: "cover",
              opacity: "0.6",
              width: "100%",
              height: "300px",
            }}
          >
            {/* search and bg */}

            <div
              className="container d-flex justify-content-center"
              style={{ height: "300px", alignItems: "center" }}
            >
              <div className="d-flex flex-column">
                <Form className=" p-0 d-flex ">
                  <FormControl
                    type="search"
                    style={{
                      border: "0",
                      color: "black",
                      width: "700px",
                      height: "50px",
                      fontSize: "18px",
                      fontWeight:"bold"
                    }}
                    placeholder="  ស្វែងរកសេវាកម្ម"
                    aria-label="Search"
                  />
                  <Button
                    style={{ border: "red", backgroundColor: "#E30000" }}
                    variant="outline-success"
                  >
                    <i class="fas fa-search" style={{ color: "white" }}></i>
                  </Button>
                </Form>

                <div className="p-0">
                  <nav className="navbar navbar-expand-lg p-0">
                    <div
                      className="collapse navbar-collapse"
                      id="navbarSupportedContent"
                    >
                      <ul className="navbar-nav mr-auto">
                        <li className="nav-item dropdown">
                          <Nav.Link
                            style={{ color: "white",fontWeight:"bold", fontSize: "18px" }}
                            className="nav-link dropdown-toggle"
                            href="#"
                            id="navbarDropdown"
                            role="button"
                            data-toggle="dropdown"
                            aria-haspopup="true"
                            aria-expanded="false"
                          >
                            ប្រភេទ
                          </Nav.Link>
                          <div
                            className="dropdown-menu"
                            aria-labelledby="navbarDropdown"
                          >
                           <Nav.Link  as={Link} to="/home/repair" className="dropdown-item" href="#" style={{fontWeight:"bold",color:"black",fontSize:"18px"}}>
                            ជួសជុល
                            </Nav.Link>
                            <Nav.Link  as={Link} to="/home/install" className="dropdown-item" href="#" style={{fontWeight:"bold",color:"black",fontSize:"18px"}}>
                            តំឡើង
                            </Nav.Link>
                          </div>
                        </li>
                        <li className="nav-item">
                        <Nav.Link
                            className="nav-link"
                            href="#"
                            style={{ color: "white",fontWeight:"bold" ,fontSize: "18px" }}
                            as={Link} to="/home/allservice"
                          >
                            សេវាកម្មទាំងអស់
                          </Nav.Link>
                          
                        </li>
                      </ul>
                    </div>
                  </nav>
                </div>
              </div>
            </div>
          </div>
        </div>
      
      {/* Body Conents */}

      <div>
         {
            // សេវាកម្មទំាងអស់
           <div>
           <div className="container" style={{ marginTop: "20px" }}>
             <h1 style={{ fontWeight: "bold" }}>សេវាកម្មទំាងអស់</h1>
           </div>  
           <Slider {...settings} >
             <div>
           <div className="container" style={{ marginTop: "20px" }}>
             <Row style={{ marginTop: "20px" }}>
               {this.state.car.map((item, index) => {
                 return (
                   <div className="col-3">
                     <Card className="shadow p-3" style={{ border: "0" }}>
                       <Card.Img variant="top" src={item.image} />
                       <Card.Body>
                         <Card.Title style={{ fontWeight: "bold" }}>
                           {item.title}
                         </Card.Title>
                         <Card.Text>{item.desc}</Card.Text>
                       </Card.Body>
                       <div
                         style={{
                           marginLeft: "15px",
                           fontSize: "20px",
                           color: "red",
                         }}
                       >
                         <span class="fa fa-star checked"></span>
                         <span class="fa fa-star checked"></span>
                         <span class="fa fa-star checked"></span>
                         <span class="fa fa-star checked"></span>
                         <span class="fa fa-star"></span>
                       </div>
                     </Card>
                   </div>
                 );
               })}
             </Row>
           </div>
             </div>
             <div> 
     
            <div className="container" style={{ marginTop: "20px" }}>
             <Row style={{ marginTop: "20px" }}>
               {this.state.car.map((item, index) => {
                 return (
                   <div className="col-3">
                     <Card className="shadow p-3" style={{ border: "0" }}>
                       <Card.Img variant="top" src={item.image} />
                       <Card.Body>
                         <Card.Title style={{ fontWeight: "bold" }}>
                           {item.title}
                         </Card.Title>
                         <Card.Text>{item.desc}</Card.Text>
                       </Card.Body>
                       <div
                         style={{
                           marginLeft: "15px",
                           fontSize: "20px",
                           color: "red",
                         }}
                       >
                         <span class="fa fa-star checked"></span>
                         <span class="fa fa-star checked"></span>
                         <span class="fa fa-star checked"></span>
                         <span class="fa fa-star checked"></span>
                         <span class="fa fa-star"></span>
                       </div>
                     </Card>
                   </div>
                 );
               })}
             </Row>
           </div>
             </div>
             <div>
               
           <div className="container" style={{ marginTop: "20px" }}>
             <Row style={{ marginTop: "20px" }}>
               {this.state.car.map((item, index) => {
                 return (
                   <div className="col-3">
                     <Card className="shadow p-3" style={{ border: "0" }}>
                       <Card.Img variant="top" src={item.image} />
                       <Card.Body>
                         <Card.Title style={{ fontWeight: "bold" }}>
                           {item.title}
                         </Card.Title>
                         <Card.Text>{item.desc}</Card.Text>
                       </Card.Body>
                       <div
                         style={{
                           marginLeft: "15px",
                           fontSize: "20px",
                           color: "red",
                         }}
                       >
                         <span class="fa fa-star checked"></span>
                         <span class="fa fa-star checked"></span>
                         <span class="fa fa-star checked"></span>
                         <span class="fa fa-star checked"></span>
                         <span class="fa fa-star"></span>
                       </div>
                     </Card>
                   </div>
                 );
               })}
             </Row>
           </div>
             </div>
           </Slider>
         </div>
        }

       </div> 
         
       <div>
         {
            // ប្រទេភសេវាកម្ម
           <div>
           <div className="container" style={{ marginTop: "20px" }}>
             <h1 style={{ fontWeight: "bold" }}>ប្រភេទសេវាកម្ម</h1>
           </div>  
           <Slider {...settings}>
             <div>
           <div className="container" style={{ marginTop: "20px" }}>
             <Row style={{ marginTop: "20px" }}>
               {this.state.car.map((item, index) => {
                 return (
                   <div className="col-3">
                     <Card className="shadow p-3" style={{ border: "0" }}>
                       <Card.Img variant="top" src={item.image} />
                       <Card.Body>
                         <Card.Title style={{ fontWeight: "bold" }}>
                           {item.title}
                         </Card.Title>
                         <Card.Text>{item.desc}</Card.Text>
                       </Card.Body>
                       <div
                         style={{
                           marginLeft: "15px",
                           fontSize: "20px",
                           color: "red",
                         }}
                       >
                         <span class="fa fa-star checked"></span>
                         <span class="fa fa-star checked"></span>
                         <span class="fa fa-star checked"></span>
                         <span class="fa fa-star checked"></span>
                         <span class="fa fa-star"></span>
                       </div>
                     </Card>
                   </div>
                 );
               })}
             </Row>
           </div>
             </div>
             <div>
     
           <div className="container" style={{ marginTop: "20px" }}>
             <Row style={{ marginTop: "20px" }}>
               {this.state.car.map((item, index) => {
                 return (
                   <div className="col-3">
                     <Card className="shadow p-3" style={{ border: "0" }}>
                       <Card.Img variant="top" src={item.image} />
                       <Card.Body>
                         <Card.Title style={{ fontWeight: "bold" }}>
                           {item.title}
                         </Card.Title>
                         <Card.Text>{item.desc}</Card.Text>
                       </Card.Body>
                       <div
                         style={{
                           marginLeft: "15px",
                           fontSize: "20px",
                           color: "red",
                         }}
                       >
                         <span class="fa fa-star checked"></span>
                         <span class="fa fa-star checked"></span>
                         <span class="fa fa-star checked"></span>
                         <span class="fa fa-star checked"></span>
                         <span class="fa fa-star"></span>
                       </div>
                     </Card>
                   </div>
                 );
               })}
             </Row>
           </div>
             </div>
             <div>
               
           <div className="container" style={{ marginTop: "20px" }}>
             <Row style={{ marginTop: "20px" }}>
               {this.state.car.map((item, index) => {
                 return (
                   <div className="col-3">
                     <Card className="shadow p-3" style={{ border: "0" }}>
                       <Card.Img variant="top" src={item.image} />
                       <Card.Body>
                         <Card.Title style={{ fontWeight: "bold" }}>
                           {item.title}
                         </Card.Title>
                         <Card.Text>{item.desc}</Card.Text>
                       </Card.Body>
                       <div
                         style={{
                           marginLeft: "15px",
                           fontSize: "20px",
                           color: "red",
                         }}
                       >
                         <span class="fa fa-star checked"></span>
                         <span class="fa fa-star checked"></span>
                         <span class="fa fa-star checked"></span>
                         <span class="fa fa-star checked"></span>
                         <span class="fa fa-star"></span>
                       </div>
                     </Card>
                   </div>
                 );
               })}
             </Row>
           </div>
             </div>
           </Slider>
         </div>
         }

       </div> 

      ​ <div >
         {
            // បណ្ដុំសេវាកម្ម
           <div>
           <div className="container" style={{ marginTop: "20px" }}>
             <h1 style={{ fontWeight: "bold" }}>បណ្ដុំសេវាកម្ម</h1>
           </div>  
           <Slider {...settings}>
             <div>
           <div className="container" style={{ marginTop: "20px" }}>
             <Row style={{ marginTop: "20px" }}>
               {this.state.car.map((item, index) => {
                 return (
                   <div className="col-3">
                     <Card className="shadow p-3" style={{ border: "0" }}>
                       <Card.Img variant="top" src={item.image} />
                       <Card.Body>
                         <Card.Title style={{ fontWeight: "bold" }}>
                           {item.title}
                         </Card.Title>
                         <Card.Text>{item.desc}</Card.Text>
                       </Card.Body>
                       <div
                         style={{
                           marginLeft: "15px",
                           fontSize: "20px",
                           color: "red",
                         }}
                       >
                         <span class="fa fa-star checked"></span>
                         <span class="fa fa-star checked"></span>
                         <span class="fa fa-star checked"></span>
                         <span class="fa fa-star checked"></span>
                         <span class="fa fa-star"></span>
                       </div>
                     </Card>
                   </div>
                 );
               })}
             </Row>
           </div>
             </div>
             <div>
     
           <div className="container" style={{ marginTop: "20px" }}>
             <Row style={{ marginTop: "20px" }}>
               {this.state.car.map((item, index) => {
                 return (
                   <div className="col-3">
                     <Card className="shadow p-3" style={{ border: "0" }}>
                       <Card.Img variant="top" src={item.image} />
                       <Card.Body>
                         <Card.Title style={{ fontWeight: "bold" }}>
                           {item.title}
                         </Card.Title>
                         <Card.Text>{item.desc}</Card.Text>
                       </Card.Body>
                       <div
                         style={{
                           marginLeft: "15px",
                           fontSize: "20px",
                           color: "red",
                         }}
                       >
                         <span class="fa fa-star checked"></span>
                         <span class="fa fa-star checked"></span>
                         <span class="fa fa-star checked"></span>
                         <span class="fa fa-star checked"></span>
                         <span class="fa fa-star"></span>
                       </div>
                     </Card>
                   </div>
                 );
               })}
             </Row>
           </div>
             </div>
             <div>
               
           <div className="container" style={{ marginTop: "20px" }}>
             <Row style={{ marginTop: "20px" }}>
               {this.state.car.map((item, index) => {
                 return (
                   <div className="col-3">
                     <Card className="shadow p-3" style={{ border: "0" }}>
                       <Card.Img variant="top" src={item.image} />
                       <Card.Body>
                         <Card.Title style={{ fontWeight: "bold" }}>
                           {item.title}
                         </Card.Title>
                         <Card.Text>{item.desc}</Card.Text>
                       </Card.Body>
                       <div
                         style={{
                           marginLeft: "15px",
                           fontSize: "20px",
                           color: "red",
                         }}
                       >
                         <span class="fa fa-star checked"></span>
                         <span class="fa fa-star checked"></span>
                         <span class="fa fa-star checked"></span>
                         <span class="fa fa-star checked"></span>
                         <span class="fa fa-star"></span>
                       </div>
                     </Card>
                   </div>
                 );
               })}
             </Row>
           </div>
             </div>
           </Slider>
         </div>
         }

       </div> 

      
      
      </div>
    );
  }
}
