import React from 'react'
import {Nav,Navbar,InputGroup,NavLink,Form,Button,Row,Card,FormControl,NavDropdown,} from "react-bootstrap";
import { Link } from "react-router-dom";

export default function Search() {
    return (
        <div>
             {/* Search */}
    
        <div
          style={{
            backgroundImage: `url("https://thumbs.dreamstime.com/b/abstract-image-wireless-network-wifi-connection-technology-concept-bangkok-city-background-night-thailand-146823201.jpg")`,
            backgroundPosition: "center",
            backgroundSize: "cover",
            width: "100%",
            height: "300px",
          }}
        >
          <div
            style={{
              backgroundColor: "black",
              backgroundRepeat: "no-repeat",
              backgroundPosition: "center",
              backgroundSize: "cover",
              opacity: "0.6",
              width: "100%",
              height: "300px",
            }}
          >
            {/* search and bg */}

            <div
              className="container d-flex justify-content-center"
              style={{ height: "300px", alignItems: "center" }}
            >
              <div className="d-flex flex-column">
                <Form className=" p-0 d-flex ">
                  <FormControl
                    type="search"
                    style={{
                      border: "0",
                      color: "black",
                      width: "700px",
                      height: "50px",
                      fontSize: "18px",
                      fontWeight:"bold",
                     
                    }}
                    placeholder="  ស្វែងរកសេវាកម្ម"
                    aria-label="Search"
                  />
                  <Button
                    style={{ border: "red", backgroundColor: "#E00000" }}
                    variant="outline-success"
                  >
                    <i class="fas fa-search" style={{ color: "white" }}></i>
                  </Button>
                </Form>

                <div className="p-0">
                  <nav className="navbar navbar-expand-lg p-0">
                    <div
                      className="collapse navbar-collapse"
                      id="navbarSupportedContent"
                    >
                      <ul className="navbar-nav mr-auto">
                        <li className="nav-item dropdown">
                          <Nav.Link
                            style={{ color: "white",fontWeight:"bold", fontSize: "18px" }}
                            className="nav-link dropdown-toggle"
                            href="#"
                            id="navbarDropdown"
                            role="button"
                            data-toggle="dropdown"
                            aria-haspopup="true"
                            aria-expanded="false"
                          >
                            ប្រភេទ
                          </Nav.Link>
                          <div
                            className="dropdown-menu"
                            aria-labelledby="navbarDropdown"
                          >
                           <Nav.Link  as={Link} to="/home/repair" className="dropdown-item" href="#" style={{fontWeight:"bold",color:"black",fontSize:"18px"}}>
                            ជួសជុល
                            </Nav.Link>
                            <Nav.Link  as={Link} to="/home/install" className="dropdown-item" href="#" style={{fontWeight:"bold",color:"black",fontSize:"18px"}}>
                            តំឡើង
                            </Nav.Link>
                          </div>
                        </li>
                        <li className="nav-item">
                        <Nav.Link
                            className="nav-link"
                            href="#"
                            style={{ color: "white",fontWeight:"bold" ,fontSize: "18px" }}
                            as={Link} to="/home/allservice"
                          >
                            សេវាកម្មទាំងអស់
                          </Nav.Link>
                          
                        </li>
                      </ul>
                    </div>
                  </nav>
                </div>
              </div>
            </div>
          </div>
        </div>
        </div>
    )
}
