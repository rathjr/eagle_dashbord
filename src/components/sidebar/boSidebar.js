/**********************************************************************
 * Development By Group: ServeNak
 * Description: business owner dashboard navbar
 **********************************************************************/
import { Link } from "react-router-dom";
import { Image } from "react-bootstrap";
import React, { useEffect, useState } from "react";
import { fetchUserById } from "../../service/user.service";
import { useParams } from "react-router-dom";
let user = JSON.parse(localStorage.getItem("user"));
console.log("usercredental :", user);
let removeItem = () => localStorage.removeItem("user");

const BoSidebar = () => {
  const [name, setName] = useState("");
  const [profile, setProfile] = useState("");
  let { id } = useParams();
  useEffect(() => {
    fetchUserById(user.id).then((response) => {
      setName(response.name);
      setProfile(response.profile);
    });
    console.log("id : ", id);
  }, []);

  return (
    <div>
      <aside
        style={{ backgroundColor: "#151719", color:"white"}}
        className="main-sidebar elevation-4"
      >
        <div className="sidebar">
          <div className="user-panel mt-3 d-flex">
            <div className="image" style={{marginTop:"8px"}}>
              {profile !== null || profile==="string" ? (
                <Image
                  src={profile}
                  roundedCircle
                  style={{ width: "45px", height: "45px", margin: "0px" }}
                />
              ) : (
                <Image
                  src="https://profiles.utdallas.edu/img/default.png"
                  roundedCircle
                  style={{ width: "45px", height: "45px", margin: "0px" }}
                />
              )}
            </div>
            <div
              className="info"
              style={{ fontSize: "25px", paddingTop: "15px" }}
            >
              <Link to="/bo/bo-setting">
                <a href="#" className="d-block" style={{ color: "#9ca9b3" }}>
                  {name}
                </a>
              </Link>
            </div>
          </div>
          <nav className="mt-2" style={{ fontSize: "22px" }}>
            <ul
              className="nav nav-pills nav-sidebar flex-column"
              data-widget="treeview"
              role="menu"
              data-accordion="false"
            >
              <li className="nav-item">
                <Link to="/bo">
                  <a href="#" className="nav-link">
                    <i className="nav-icon fas fa-home" />
                    <p>ទំព័រដើម</p>
                  </a>
                </Link>
              </li>
              <li className="nav-item">
                <Link to="/bo/player">
                  <a href="pages/widgets.html" className="nav-link">
                    <i className="nav-icon fas fa-bullhorn" />
                    <p>កីឡាករ</p>
                  </a>
                </Link>
              </li>
              <li className="nav-item">
                <Link to="/bo/match">
                  <a href="pages/widgets.html" className="nav-link">
                    <i className="nav-icon fas fa-bullhorn" />
                    <p>ការប្រកួត</p>
                  </a>
                </Link>
              </li>
              <li className="nav-item">
                <Link to="/sign-in" onClick={removeItem}>
                  <a href="pages/widgets.html" className="nav-link">
                    <i className="nav-icon fas fa-sign-out-alt" />
                    <p>ចាកចេញ</p>
                  </a>
                </Link>
              </li>
            </ul>
          </nav>
        </div>
      </aside>
    </div>
  );
};

export default BoSidebar;
