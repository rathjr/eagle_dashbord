/**********************************************************************
 * Development By Group: ServeNak
 * Description: 404 Not Found Page
 **********************************************************************/

 import React from 'react'
 
 const NotFound = () => {
     return (
         <div style={{backgroundColor:"#F5F5F5"}}>
             <div className="text-center container pt-5 pb-5  " style={{minHeight:"75vh"}} >
                <img style={{paddingTop:"80px"}} className="img-fluid" width="500px" src="../../images/404.png"></img>
                <h1 style={{fontSize:"80px",textAlign:"center",color:"#E30000"}}> ៤០៤  ស្វែងរកមិនឃើញ!</h1>
             </div>
         </div>
     )
 }
 
 export default NotFound