import React, { Component, useRef, useState } from 'react'
import ReactStars from 'react-rating-stars-component'
import { Container, Row, Col, Card, Button, Image, OverlayTrigger, Popover, Modal, Form } from 'react-bootstrap'
import NavMenu from '../Home/Navbar'
import Footer from '../Home/Footer'

export default function ServiceDetail() {
    const ratingChanged = (newRating) => {
        console.log(newRating);
    };
    function MyVerticallyCenteredModal(props) {
        return (
            <Modal
                {...props}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter" style={{ fontFamily: "nokora-bold" }}>
                        ជួសជួលកុំព្យូទ័រ ពេញចិត្ត
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <h5 style={{ fontFamily: "nokora-bold" }}>តើអ្នកពេញចិត្តសេវាកម្មមួយនេះកម្រិតណា?</h5>
                    <ReactStars
                        count={5}
                        onChange={ratingChanged}
                        size={40}
                        isHalf={true}
                        emptyIcon={<i className="far fa-star"></i>}
                        halfIcon={<i className="fa fa-star-half-alt"></i>}
                        fullIcon={<i className="fa fa-star"></i>}
                        activeColor="#E00000"
                    />
                    <div className="mt-2">
                        <h5 style={{ fontFamily: "nokora-bold" }}>ផ្ដល់់មតិយោបល់លើសេវាកម្ម</h5><Form.Control
                            as="textarea"
                            style={{ height: '100px' }}
                        />
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={props.onHide} style={{ fontFamily: "nokora" }}>បញ្ជូន</Button>
                </Modal.Footer>
            </Modal>
        );
    }

    const popover = (
        <Popover id="popover-basic" style={{ width: "150px" }}>
            <div className="m-3" style={{ fontFamily: "nokora-bold", fontSize: "17px" }}>ចែករំលែកជាមួយ</div>
            <hr />
            <span style={{ marginLeft: "15px", fontSize: "30px", color: "#3b5998" }}>
                <i class="bi bi-facebook" style={{ href: "#" }}></i>
            </span>
            <span style={{ marginLeft: "15px", fontSize: "30px", color: "#0088cc" }}>
                <i class="bi bi-telegram"></i>
            </span>
            <span style={{ marginLeft: "15px", fontSize: "30px", color: "#ac2bac" }}>
                <i class="bi bi-instagram"></i>
            </span>
        </Popover>
    );

    const [modalShow, setModalShow] = React.useState(false);

    return (
        <div style={{ backgroundColor: "white-smoke" }}>
            <NavMenu />
            <div style={{
                backgroundImage: `url(${process.env.PUBLIC_URL + "../images/1.computer.jpg"})`,
                backgroundPosition: "center",
                backgroundSize: "cover",
                width: "100%",
                height: "300px",
            }}>
                <Container style={{ paddingTop: "170px" }}>
                    <h1 style={{ fontFamily: "nokora-bold", color: "white" }}>ជួសជួលកុំព្យូទ័រ ពេញចិត្ត</h1>
                    <div
                        style={{
                            fontSize: "20px",
                            color: "red",
                            paddingBottom: "5px"
                        }}
                    >
                        <span class="fa fa-star "></span>
                        <span class="fa fa-star "></span>
                        <span class="fa fa-star "></span>
                        <span class="fa fa-star "></span>
                        <span class="fa fa-star"></span>
                        <span style={{ fontFamily: "poppins-regular", color: "black", fontSize: "15px" }}> 100 reviews</span>
                    </div>
                    <div style={{ fontFamily: "nokora", color: "white" }}>បើក ០៧:០០​ ព្រឹក​ ដល់ ០៨:០០ យប់</div>
                </Container>
            </div>
            <Container >
                <Row className="mt-3" >
                    <Col md={8}>
                        <Card className="shadow p-3" style={{ border: "0" }}>
                            <Card.Body>
                                <h3 style={{ fontFamily: "nokora-bold" }}>អំពីសេវាកម្មរបស់យើងខ្ញុំ</h3>
                                <hr />
                                <Card.Text style={{ fontFamily: "nokora", fontSize: "18px", textAlign: "justify" }}>
                                    ពិបាកក្នុងការស្វែងរកសេវាកម្មជួសជុលកុំព្យូទ័រដែលអាចទុកចិត្តបានមែនទេ? មកទីនេះលោកអ្នកនឹងមិនខកបំណងឡើយ ពួកយើងមានសេវាកម្មជួសជុល​និងតម្លើងកុំព្យូទ័គ្រប់ស៊េរី ម៉ូដែល។ លើសពីនេះលោកអ្នកនិងទទួលបានការធានាក្រោយពីទទួលបានសេវាកម្មពីយើងខ្ញុំក្នុងរយះពេលសមស្របមួយ។
                                </Card.Text>
                                <div className="d-flex justify-content-end mx-3" style={{ fontFamily: "nokora-bold" }}>
                                    <Button
                                        onClick={() => setModalShow(true)}
                                        className="shadow p-3"
                                        style={{
                                            border: "0",
                                            borderRadius: "10px",
                                            width: "120px", height: "50px",
                                            marginBottom: "10px",
                                            marginRight: "10px"
                                        }}
                                        variant="light"
                                    >
                                        <img style={{ width: "20px", marginRight: "5px" }} src="https://img.icons8.com/material-outlined/24/fa314a/popular-topic.png" />
                                        វាយតម្លៃ
                                    </Button>
                                    <MyVerticallyCenteredModal
                                        show={modalShow}
                                        onHide={() => setModalShow(false)}
                                    />
                                    <OverlayTrigger trigger="click" placement="bottom" overlay={popover}>
                                        <Button
                                            className="shadow p-3"
                                            style={{
                                                border: "0",
                                                borderRadius: "10px",
                                                width: "120px",
                                                height: "50px",
                                                marginBottom: "10px",
                                                marginRight: "10px"
                                            }}
                                            variant="light"
                                        >
                                            <img style={{ width: "20px", marginRight: "5px", marginBottom: "2px" }} src="https://img.icons8.com/ios-glyphs/30/fa314a/share--v1.png" />
                                            ចែករំលែក
                                        </Button>
                                    </OverlayTrigger>
                                    <Button
                                        className="shadow p-3"
                                        style={{
                                            border: "0",
                                            borderRadius: "10px",
                                            width: "120px",
                                            height: "50px",
                                            marginBottom: "10px",
                                            marginRight: "10px"
                                        }}
                                        variant="light"
                                    >
                                        <img style={{ width: "18px", marginRight: "4px", marginBottom: "2px" }} src="https://img.icons8.com/glyph-neue/64/fa314a/likes-folder.png" />
                                        ចំណូលចិត្ត
                                    </Button>
                                        
                                </div>
                                <div style={{ fontFamily: "nokora-bold" }}>
                                    <h5>ការវាយតម្លៃ និងផ្ដល់មតិយោបល់</h5>
                                </div>
                                <hr />
                                <Row >
                                    <Col md={1}>
                                        <Image
                                            src="../images/img_1809 copy.jpg" roundedCircle
                                            style={{ width: "50px", height: "50px" }}
                                        >
                                        </Image>
                                    </Col>
                                    <Col md={6} style={{ paddingTop: "5px", fontFamily: "nokora", marginLeft: "10px" }}>
                                        <div style={{ fontSize: "12px" }}>២២​ មេសា ២០២១</div>
                                        <div style={{ fontFamily: "nokora-bold" }}>ជឹមយ៉ុង គឹមហាក់</div>
                                        <div>
                                            សេវាកម្មល្អ ចំណាយពេលតិច ផ្ដល់់ទំនុកចិត្តដល់អតិថិជន
                                        </div>
                                    </Col>
                                    <Col md={4}>
                                        <div
                                            style={{
                                                fontSize: "20px",
                                                color: "red",
                                                paddingBottom: "5px",
                                                textAlign: "right"
                                            }}
                                        >
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star"></span>
                                        </div>
                                    </Col>
                                </Row>{''}
                                <Row className="my-3">
                                    <Col md={1}>
                                        <Image
                                            src="../images/img_1809 copy.jpg" roundedCircle
                                            style={{ width: "50px", height: "50px" }}
                                        >
                                        </Image>
                                    </Col>
                                    <Col md={6} style={{ paddingTop: "5px", fontFamily: "nokora", marginLeft: "10px" }}>
                                        <div style={{ fontSize: "12px" }}>២២​ មេសា ២០២១</div>
                                        <div style={{ fontFamily: "nokora-bold" }}>ជឹមយ៉ុង គឹមហាក់</div>
                                        <div>
                                            សេវាកម្មល្អ ចំណាយពេលតិច ផ្ដល់់ទំនុកចិត្តដល់អតិថិជន
                                        </div>
                                    </Col>
                                    <Col md={4}>
                                        <div
                                            style={{
                                                fontSize: "20px",
                                                color: "red",
                                                paddingBottom: "5px",
                                                textAlign: "right"
                                            }}
                                        >
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star"></span>
                                        </div>
                                    </Col>
                                </Row>{''}
                                <Row className="my-3">
                                    <Col md={1}>
                                        <Image
                                            src="../images/img_1809 copy.jpg" roundedCircle
                                            style={{ width: "50px", height: "50px" }}
                                        >
                                        </Image>
                                    </Col>
                                    <Col md={6} style={{ paddingTop: "5px", fontFamily: "nokora", marginLeft: "10px" }}>
                                        <div style={{ fontSize: "12px" }} >២២​ មេសា ២០២១</div>
                                        <div style={{ fontFamily: "nokora-bold" }}>ជឹមយ៉ុង គឹមហាក់</div>
                                        <div>
                                            សេវាកម្មល្អ ចំណាយពេលតិច ផ្ដល់់ទំនុកចិត្តដល់អតិថិជន
                                        </div>
                                    </Col>
                                    <Col md={4}>
                                        <div
                                            style={{
                                                fontSize: "20px",
                                                color: "red",
                                                paddingBottom: "5px",
                                                textAlign: "right"
                                            }}
                                        >
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star"></span>
                                        </div>
                                    </Col>
                                </Row>{''}
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col md={4}>
                        <Card className="shadow p-3" style={{ border: "0" }}>
                            <Card.Body>
                                <h5 style={{ fontFamily: "nokora-bold" }}>ទីតាំង​ និង ទំនាក់ទំនង</h5>
                                <hr />
                                {/* Google map */}
                                <div>
                                    <iframe style={{ width: "300px" }} src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3908.6647761043223!2d104.8893663019937!3d11.57587188111183!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3109519cfe734349%3A0x5174d2178ff95112!2sKorean%20HRD%20Center!5e0!3m2!1sen!2skh!4v1627535061765!5m2!1sen!2skh" />

                                    <Row>
                                        <Col md={8}>
                                            <p
                                                style={{ fontFamily: "poppins-regular", fontSize: "12px", textDecoration: "none" }}
                                                href="#!"
                                                class="text-reset">
                                                {/* ផ្ទះ​លេខ ១២ ផ្លូវលេខ ៣២៣ សង្កាត់ បឹងកក់​ II ខណ្ឌ ទួលគោក រាជធានី ភ្នំពេញ ព្រះរាជាណាចក្រកម្ពុជា */}
                                                No.12, St.323, Boueng Kak II Commune, Toul Kork District, Phnom Penh Cambodia
                                            </p>
                                        </Col>
                                        <Col md={4} >
                                            <div style={{ paddingRight: "50px" }}>
                                                <Button
                                                    className="shadow p-3"
                                                    style={{
                                                        border: "0",
                                                        borderRadius: "10px",
                                                        width: "100px",
                                                        color: "red",
                                                        fontFamily: "nokora-bold",
                                                        fontSize: "10px",
                                                    }}
                                                    variant="light">
                                                    ទទួលនូវគោលដៅ
                                                </Button>
                                            </div>
                                        </Col>
                                    </Row>
                                </div>
                                <div>
                                    <Row>
                                        <Col md={4} style={{ fontFamily: "nokora-bold", fontSize: "20px" }}><h5>ទូរស័ព្ទ</h5></Col>
                                        <Col md={8}><h6 style={{ fontFamily: "poppins-regular" }}>023-333-333</h6></Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} style={{ fontFamily: "nokora-bold", fontSize: "20px" }}><h5>អ៊ីម៉ែល</h5></Col>
                                        <Col md={8}><h6 style={{ fontFamily: "poppins-regular" }}>servenak@gmail.com</h6></Col>
                                    </Row>
                                    <Row>
                                        <Col md={6} style={{ fontFamily: "nokora-bold", fontSize: "20px" }}><h5 style={{ fontFamily: "nokora-bold", fontSize: "20px" }}>បណ្ដាញសង្គម</h5></Col>
                                    </Row>
                                    <Row>
                                        <Col md={6} >
                                            <span style={{ marginLeft: "15px", fontSize: "30px", color: "#3b5998" }}>
                                                <i class="bi bi-facebook" style={{ href: "#" }}></i>
                                            </span>
                                            <span style={{ marginLeft: "15px", fontSize: "30px", color: "#0088cc" }}>
                                                <i class="bi bi-telegram"></i>
                                            </span>
                                            <span style={{ marginLeft: "15px", fontSize: "30px", color: "#ac2bac" }}>
                                                <i class="bi bi-instagram"></i>
                                            </span>
                                        </Col>
                                    </Row>
                                </div>
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
            </Container>
            <Footer />
        </div>
    )

}

