import axios from 'axios';
const api = axios.create({
    baseURL: 'http://3.144.75.173:17000/api/',
});

export default api;