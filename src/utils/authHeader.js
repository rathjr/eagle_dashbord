import StringCrypto from "string-crypto";

const {
    decryptString
  } = new StringCrypto;
  export const authHeader = () => {
    let user = JSON.parse(localStorage.getItem('user'))
    if (user && user.token) {
        let token = decryptString(user.token, process.env.REACT_APP_SECRET)
        return {
            ContentType: "application/json",
            Authorization: 'Bearer ' + token,
        };
    } else {
        return { ContentType: "application/json" }
    }
}
// decrype token to athorize