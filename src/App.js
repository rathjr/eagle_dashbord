/**********************************************************************
 * Development By Group: ServeNak
 * Description: main route
 **********************************************************************/

import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import "./assets/Style/App.css";
import bo from "./router/bo.js";
import Login from "./views/ui/Login";
import SignUp from "./views/ui/SignUp";


function App() {
  return (
    <div className="myfont">
      <Router>
        <Switch>
          <Route path="/bo" component={bo} />
          {/* <Route path="/admin" component={admin} />
          <Route path="/home" component={ui} /> */}
          <Route path="/sign-in" component={Login} />
          <Route path="/sign-up" component={SignUp} />
          {/* <Route path="/choose-role" component={ChooseRole} />
          <Route path="/view-detail/:id" component={ViewDetail} />
          <Route path="/" component={HomePage} /> */}
        </Switch>
      </Router>
    </div>
  );
}
export default App;
