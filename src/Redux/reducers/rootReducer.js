import { combineReducers } from "redux";
import postReducer from "./postReducer";
import userReducer from "./userReducer";
const reducers = combineReducers({
  userReducer,postReducer
});
export default reducers;
